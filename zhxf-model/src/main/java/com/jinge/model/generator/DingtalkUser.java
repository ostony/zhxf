package com.jinge.model.generator;

import java.io.Serializable;

import javax.persistence.*;

@Table(name = "dingtalk_user")
public class DingtalkUser implements Serializable{
    /**  
     * serialVersionUID:TODO(用一句话描述这个变量表示什么).  
     */
    private static final long serialVersionUID = 751626624663764387L;

    /**
     * 主键
     */
    @Id
    @GeneratedValue(generator = "JDBC")
    private String id;

    /**
     * 员工在当前开发者企业账号范围内的唯一标识
     */
    private String unionid;

    /**
     * 员工在当前企业内的唯一标识，也称staffId。可由企业在创建时指定，并代表一定含义比如工号
     */
    private String userid;

    /**
     * 员工名字
     */
    private String name;

    /**
     * 办公地点
     */
    @Column(name = "work_place")
    private String workPlace;

    /**
     * 备注
     */
    private String remark;

    /**
     * 手机号码
     */
    private String mobile;

    /**
     * 员工的电子邮箱
     */
    private String email;

    /**
     * 员工的企业邮箱
     */
    @Column(name = "org_email")
    private String orgEmail;

    /**
     * 头像url
     */
    private String avatar;

    /**
     * 是否已经激活，1表示已激活，0表示未激活
     */
    private Integer active;

    /**
     * 职位信息
     */
    private String position;

    /**
     * 国家地区码
     */
    @Column(name = "state_code")
    private String stateCode;

    /**
     * 用户昵称
     */
    private String nick;

    /**
     * 用户在当前开放应用内的唯一标识
     */
    private String openid;

    /**
     * 获取主键
     *
     * @return id - 主键
     */
    public String getId() {
        return id;
    }

    public DingtalkUser withId(String id) {
        this.setId(id);
        return this;
    }

    /**
     * 设置主键
     *
     * @param id 主键
     */
    public void setId(String id) {
        this.id = id;
    }

    /**
     * 获取员工在当前开发者企业账号范围内的唯一标识
     *
     * @return unionid - 员工在当前开发者企业账号范围内的唯一标识
     */
    public String getUnionid() {
        return unionid;
    }

    public DingtalkUser withUnionid(String unionid) {
        this.setUnionid(unionid);
        return this;
    }

    /**
     * 设置员工在当前开发者企业账号范围内的唯一标识
     *
     * @param unionid 员工在当前开发者企业账号范围内的唯一标识
     */
    public void setUnionid(String unionid) {
        this.unionid = unionid;
    }

    /**
     * 获取员工在当前企业内的唯一标识，也称staffId。可由企业在创建时指定，并代表一定含义比如工号
     *
     * @return userid - 员工在当前企业内的唯一标识，也称staffId。可由企业在创建时指定，并代表一定含义比如工号
     */
    public String getUserid() {
        return userid;
    }

    public DingtalkUser withUserid(String userid) {
        this.setUserid(userid);
        return this;
    }

    /**
     * 设置员工在当前企业内的唯一标识，也称staffId。可由企业在创建时指定，并代表一定含义比如工号
     *
     * @param userid 员工在当前企业内的唯一标识，也称staffId。可由企业在创建时指定，并代表一定含义比如工号
     */
    public void setUserid(String userid) {
        this.userid = userid;
    }

    /**
     * 获取员工名字
     *
     * @return name - 员工名字
     */
    public String getName() {
        return name;
    }

    public DingtalkUser withName(String name) {
        this.setName(name);
        return this;
    }

    /**
     * 设置员工名字
     *
     * @param name 员工名字
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取办公地点
     *
     * @return work_place - 办公地点
     */
    public String getWorkPlace() {
        return workPlace;
    }

    public DingtalkUser withWorkPlace(String workPlace) {
        this.setWorkPlace(workPlace);
        return this;
    }

    /**
     * 设置办公地点
     *
     * @param workPlace 办公地点
     */
    public void setWorkPlace(String workPlace) {
        this.workPlace = workPlace;
    }

    /**
     * 获取备注
     *
     * @return remark - 备注
     */
    public String getRemark() {
        return remark;
    }

    public DingtalkUser withRemark(String remark) {
        this.setRemark(remark);
        return this;
    }

    /**
     * 设置备注
     *
     * @param remark 备注
     */
    public void setRemark(String remark) {
        this.remark = remark;
    }

    /**
     * 获取手机号码
     *
     * @return mobile - 手机号码
     */
    public String getMobile() {
        return mobile;
    }

    public DingtalkUser withMobile(String mobile) {
        this.setMobile(mobile);
        return this;
    }

    /**
     * 设置手机号码
     *
     * @param mobile 手机号码
     */
    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    /**
     * 获取员工的电子邮箱
     *
     * @return email - 员工的电子邮箱
     */
    public String getEmail() {
        return email;
    }

    public DingtalkUser withEmail(String email) {
        this.setEmail(email);
        return this;
    }

    /**
     * 设置员工的电子邮箱
     *
     * @param email 员工的电子邮箱
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * 获取员工的企业邮箱
     *
     * @return org_email - 员工的企业邮箱
     */
    public String getOrgEmail() {
        return orgEmail;
    }

    public DingtalkUser withOrgEmail(String orgEmail) {
        this.setOrgEmail(orgEmail);
        return this;
    }

    /**
     * 设置员工的企业邮箱
     *
     * @param orgEmail 员工的企业邮箱
     */
    public void setOrgEmail(String orgEmail) {
        this.orgEmail = orgEmail;
    }

    /**
     * 获取头像url
     *
     * @return avatar - 头像url
     */
    public String getAvatar() {
        return avatar;
    }

    public DingtalkUser withAvatar(String avatar) {
        this.setAvatar(avatar);
        return this;
    }

    /**
     * 设置头像url
     *
     * @param avatar 头像url
     */
    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    /**
     * 获取是否已经激活，1表示已激活，0表示未激活
     *
     * @return active - 是否已经激活，1表示已激活，0表示未激活
     */
    public Integer getActive() {
        return active;
    }

    public DingtalkUser withActive(Integer active) {
        this.setActive(active);
        return this;
    }

    /**
     * 设置是否已经激活，1表示已激活，0表示未激活
     *
     * @param active 是否已经激活，1表示已激活，0表示未激活
     */
    public void setActive(Integer active) {
        this.active = active;
    }

    /**
     * 获取职位信息
     *
     * @return position - 职位信息
     */
    public String getPosition() {
        return position;
    }

    public DingtalkUser withPosition(String position) {
        this.setPosition(position);
        return this;
    }

    /**
     * 设置职位信息
     *
     * @param position 职位信息
     */
    public void setPosition(String position) {
        this.position = position;
    }

    /**
     * 获取国家地区码
     *
     * @return state_code - 国家地区码
     */
    public String getStateCode() {
        return stateCode;
    }

    public DingtalkUser withStateCode(String stateCode) {
        this.setStateCode(stateCode);
        return this;
    }

    /**
     * 设置国家地区码
     *
     * @param stateCode 国家地区码
     */
    public void setStateCode(String stateCode) {
        this.stateCode = stateCode;
    }

    /**
     * 获取用户昵称
     *
     * @return nick - 用户昵称
     */
    public String getNick() {
        return nick;
    }

    public DingtalkUser withNick(String nick) {
        this.setNick(nick);
        return this;
    }

    /**
     * 设置用户昵称
     *
     * @param nick 用户昵称
     */
    public void setNick(String nick) {
        this.nick = nick;
    }

    /**
     * 获取用户在当前开放应用内的唯一标识
     *
     * @return openid - 用户在当前开放应用内的唯一标识
     */
    public String getOpenid() {
        return openid;
    }

    public DingtalkUser withOpenid(String openid) {
        this.setOpenid(openid);
        return this;
    }

    /**
     * 设置用户在当前开放应用内的唯一标识
     *
     * @param openid 用户在当前开放应用内的唯一标识
     */
    public void setOpenid(String openid) {
        this.openid = openid;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", unionid=").append(unionid);
        sb.append(", userid=").append(userid);
        sb.append(", name=").append(name);
        sb.append(", workPlace=").append(workPlace);
        sb.append(", remark=").append(remark);
        sb.append(", mobile=").append(mobile);
        sb.append(", email=").append(email);
        sb.append(", orgEmail=").append(orgEmail);
        sb.append(", avatar=").append(avatar);
        sb.append(", active=").append(active);
        sb.append(", position=").append(position);
        sb.append(", stateCode=").append(stateCode);
        sb.append(", nick=").append(nick);
        sb.append(", openid=").append(openid);
        sb.append("]");
        return sb.toString();
    }
}