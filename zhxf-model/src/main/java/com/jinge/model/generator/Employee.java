package com.jinge.model.generator;

import java.io.Serializable;

import javax.persistence.*;

@Table(name = "employee")
public class Employee implements Serializable{
    /**  
     * serialVersionUID:TODO(用一句话描述这个变量表示什么).  
     */
    private static final long serialVersionUID = 124096636602639093L;

    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 员工编号
     */
    private String number;

    /**
     * 员工姓名
     */
    private String name;

    /**
     * 所在部门
     */
    @Column(name = "dept_name")
    private String deptName;

    /**
     * 基本工资
     */
    @Column(name = "base_pay")
    private Integer basePay;

    /**
     * 奖金
     */
    private Integer bonus;

    /**
     * 住房补助
     */
    @Column(name = "housing_allowance")
    private Integer housingAllowance;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    public Employee withId(Integer id) {
        this.setId(id);
        return this;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取员工编号
     *
     * @return number - 员工编号
     */
    public String getNumber() {
        return number;
    }

    public Employee withNumber(String number) {
        this.setNumber(number);
        return this;
    }

    /**
     * 设置员工编号
     *
     * @param number 员工编号
     */
    public void setNumber(String number) {
        this.number = number;
    }

    /**
     * 获取员工姓名
     *
     * @return name - 员工姓名
     */
    public String getName() {
        return name;
    }

    public Employee withName(String name) {
        this.setName(name);
        return this;
    }

    /**
     * 设置员工姓名
     *
     * @param name 员工姓名
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取所在部门
     *
     * @return dept_name - 所在部门
     */
    public String getDeptName() {
        return deptName;
    }

    public Employee withDeptName(String deptName) {
        this.setDeptName(deptName);
        return this;
    }

    /**
     * 设置所在部门
     *
     * @param deptName 所在部门
     */
    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    /**
     * 获取基本工资
     *
     * @return base_pay - 基本工资
     */
    public Integer getBasePay() {
        return basePay;
    }

    public Employee withBasePay(Integer basePay) {
        this.setBasePay(basePay);
        return this;
    }

    /**
     * 设置基本工资
     *
     * @param basePay 基本工资
     */
    public void setBasePay(Integer basePay) {
        this.basePay = basePay;
    }

    /**
     * 获取奖金
     *
     * @return bonus - 奖金
     */
    public Integer getBonus() {
        return bonus;
    }

    public Employee withBonus(Integer bonus) {
        this.setBonus(bonus);
        return this;
    }

    /**
     * 设置奖金
     *
     * @param bonus 奖金
     */
    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    /**
     * 获取住房补助
     *
     * @return housing_allowance - 住房补助
     */
    public Integer getHousingAllowance() {
        return housingAllowance;
    }

    public Employee withHousingAllowance(Integer housingAllowance) {
        this.setHousingAllowance(housingAllowance);
        return this;
    }

    /**
     * 设置住房补助
     *
     * @param housingAllowance 住房补助
     */
    public void setHousingAllowance(Integer housingAllowance) {
        this.housingAllowance = housingAllowance;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", number=").append(number);
        sb.append(", name=").append(name);
        sb.append(", deptName=").append(deptName);
        sb.append(", basePay=").append(basePay);
        sb.append(", bonus=").append(bonus);
        sb.append(", housingAllowance=").append(housingAllowance);
        sb.append("]");
        return sb.toString();
    }
}