package com.jinge.model.generator;

import java.io.Serializable;

import javax.persistence.*;

@Table(name = "dept")
public class Dept implements Serializable{
    /**  
     * serialVersionUID:TODO(用一句话描述这个变量表示什么).  
     */
    private static final long serialVersionUID = -4618284731629093255L;

    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 部门名称
     */
    private String name;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    public Dept withId(Integer id) {
        this.setId(id);
        return this;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取部门名称
     *
     * @return name - 部门名称
     */
    public String getName() {
        return name;
    }

    public Dept withName(String name) {
        this.setName(name);
        return this;
    }

    /**
     * 设置部门名称
     *
     * @param name 部门名称
     */
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append("]");
        return sb.toString();
    }
}