package com.jinge.model.generator;

import java.io.Serializable;

import javax.persistence.*;

@Table(name = "role")
public class Role implements Serializable{
    /**  
     * serialVersionUID:TODO(用一句话描述这个变量表示什么).  
     */
    private static final long serialVersionUID = -5707947075283221441L;

    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 角色名称
     */
    private String name;

    /**
     * 角色描述
     */
    private String description;

    /**
     * 菜单权限列表，起止逗号并以逗号隔开
     */
    @Column(name = "menu_codes")
    private String menuCodes;

    /**
     * 后台数据权限
     */
    private String secured;

    /**
     * 是否删除 0未删除 1已删除
     */
    private Integer isdelete;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    public Role withId(Integer id) {
        this.setId(id);
        return this;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取角色名称
     *
     * @return name - 角色名称
     */
    public String getName() {
        return name;
    }

    public Role withName(String name) {
        this.setName(name);
        return this;
    }

    /**
     * 设置角色名称
     *
     * @param name 角色名称
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取角色描述
     *
     * @return description - 角色描述
     */
    public String getDescription() {
        return description;
    }

    public Role withDescription(String description) {
        this.setDescription(description);
        return this;
    }

    /**
     * 设置角色描述
     *
     * @param description 角色描述
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * 获取菜单权限列表，起止逗号并以逗号隔开
     *
     * @return menu_codes - 菜单权限列表，起止逗号并以逗号隔开
     */
    public String getMenuCodes() {
        return menuCodes;
    }

    public Role withMenuCodes(String menuCodes) {
        this.setMenuCodes(menuCodes);
        return this;
    }

    /**
     * 设置菜单权限列表，起止逗号并以逗号隔开
     *
     * @param menuCodes 菜单权限列表，起止逗号并以逗号隔开
     */
    public void setMenuCodes(String menuCodes) {
        this.menuCodes = menuCodes;
    }

    /**
     * 获取后台数据权限
     *
     * @return secured - 后台数据权限
     */
    public String getSecured() {
        return secured;
    }

    public Role withSecured(String secured) {
        this.setSecured(secured);
        return this;
    }

    /**
     * 设置后台数据权限
     *
     * @param secured 后台数据权限
     */
    public void setSecured(String secured) {
        this.secured = secured;
    }

    /**
     * 获取是否删除 0未删除 1已删除
     *
     * @return isdelete - 是否删除 0未删除 1已删除
     */
    public Integer getIsdelete() {
        return isdelete;
    }

    public Role withIsdelete(Integer isdelete) {
        this.setIsdelete(isdelete);
        return this;
    }

    /**
     * 设置是否删除 0未删除 1已删除
     *
     * @param isdelete 是否删除 0未删除 1已删除
     */
    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", name=").append(name);
        sb.append(", description=").append(description);
        sb.append(", menuCodes=").append(menuCodes);
        sb.append(", secured=").append(secured);
        sb.append(", isdelete=").append(isdelete);
        sb.append("]");
        return sb.toString();
    }
}