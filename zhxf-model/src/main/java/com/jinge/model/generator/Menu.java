package com.jinge.model.generator;

import javax.persistence.*;

@Table(name = "menu")
public class Menu {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 菜单编码，排序及父子关系，两位为一层
     */
    private String code;

    /**
     * 菜单名称；title和name都用这个
     */
    private String title;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 菜单链接
     */
    private String path;

    /**
     * 二级链接
     */
    @Column(name = "second_path")
    private String secondPath;

    /**
     * 菜单页面路径
     */
    private String component;

    /**
     * 重定向地址，例如首页路径会自动重定向dashboard
     */
    private String redirect;

    /**
     * 是否删除 0未删除 1已删除
     */
    private Integer isdelete;

    /**
     * 菜单所需权限
     */
    private String authority;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    public Menu withId(Integer id) {
        this.setId(id);
        return this;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取菜单编码，排序及父子关系，两位为一层
     *
     * @return code - 菜单编码，排序及父子关系，两位为一层
     */
    public String getCode() {
        return code;
    }

    public Menu withCode(String code) {
        this.setCode(code);
        return this;
    }

    /**
     * 设置菜单编码，排序及父子关系，两位为一层
     *
     * @param code 菜单编码，排序及父子关系，两位为一层
     */
    public void setCode(String code) {
        this.code = code;
    }

    /**
     * 获取菜单名称；title和name都用这个
     *
     * @return title - 菜单名称；title和name都用这个
     */
    public String getTitle() {
        return title;
    }

    public Menu withTitle(String title) {
        this.setTitle(title);
        return this;
    }

    /**
     * 设置菜单名称；title和name都用这个
     *
     * @param title 菜单名称；title和name都用这个
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取菜单图标
     *
     * @return icon - 菜单图标
     */
    public String getIcon() {
        return icon;
    }

    public Menu withIcon(String icon) {
        this.setIcon(icon);
        return this;
    }

    /**
     * 设置菜单图标
     *
     * @param icon 菜单图标
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 获取菜单链接
     *
     * @return path - 菜单链接
     */
    public String getPath() {
        return path;
    }

    public Menu withPath(String path) {
        this.setPath(path);
        return this;
    }

    /**
     * 设置菜单链接
     *
     * @param path 菜单链接
     */
    public void setPath(String path) {
        this.path = path;
    }

    /**
     * 获取二级链接
     *
     * @return second_path - 二级链接
     */
    public String getSecondPath() {
        return secondPath;
    }

    public Menu withSecondPath(String secondPath) {
        this.setSecondPath(secondPath);
        return this;
    }

    /**
     * 设置二级链接
     *
     * @param secondPath 二级链接
     */
    public void setSecondPath(String secondPath) {
        this.secondPath = secondPath;
    }

    /**
     * 获取菜单页面路径
     *
     * @return component - 菜单页面路径
     */
    public String getComponent() {
        return component;
    }

    public Menu withComponent(String component) {
        this.setComponent(component);
        return this;
    }

    /**
     * 设置菜单页面路径
     *
     * @param component 菜单页面路径
     */
    public void setComponent(String component) {
        this.component = component;
    }

    /**
     * 获取重定向地址，例如首页路径会自动重定向dashboard
     *
     * @return redirect - 重定向地址，例如首页路径会自动重定向dashboard
     */
    public String getRedirect() {
        return redirect;
    }

    public Menu withRedirect(String redirect) {
        this.setRedirect(redirect);
        return this;
    }

    /**
     * 设置重定向地址，例如首页路径会自动重定向dashboard
     *
     * @param redirect 重定向地址，例如首页路径会自动重定向dashboard
     */
    public void setRedirect(String redirect) {
        this.redirect = redirect;
    }

    /**
     * 获取是否删除 0未删除 1已删除
     *
     * @return isdelete - 是否删除 0未删除 1已删除
     */
    public Integer getIsdelete() {
        return isdelete;
    }

    public Menu withIsdelete(Integer isdelete) {
        this.setIsdelete(isdelete);
        return this;
    }

    /**
     * 设置是否删除 0未删除 1已删除
     *
     * @param isdelete 是否删除 0未删除 1已删除
     */
    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    /**
     * 获取菜单所需权限
     *
     * @return authority - 菜单所需权限
     */
    public String getAuthority() {
        return authority;
    }

    public Menu withAuthority(String authority) {
        this.setAuthority(authority);
        return this;
    }

    /**
     * 设置菜单所需权限
     *
     * @param authority 菜单所需权限
     */
    public void setAuthority(String authority) {
        this.authority = authority;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", code=").append(code);
        sb.append(", title=").append(title);
        sb.append(", icon=").append(icon);
        sb.append(", path=").append(path);
        sb.append(", secondPath=").append(secondPath);
        sb.append(", component=").append(component);
        sb.append(", redirect=").append(redirect);
        sb.append(", isdelete=").append(isdelete);
        sb.append(", authority=").append(authority);
        sb.append("]");
        return sb.toString();
    }
}