package com.jinge.model.generator;

import javax.persistence.*;

@Table(name = "sys_menu")
public class SysMenu {
    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 菜单父id
     */
    @Column(name = "parent_id")
    private Integer parentId;

    /**
     * 菜单图标
     */
    private String icon;

    /**
     * 菜单标题
     */
    private String title;

    /**
     * 菜单外部链接
     */
    private String href;

    /**
     * 菜单链接
     */
    @Column(name = "lay_href")
    private String layHref;

    /**
     * 菜单事件
     */
    @Column(name = "layadmin_event")
    private String layadminEvent;

    /**
     * 菜单展开页标题
     */
    @Column(name = "lay_text")
    private String layText;

    /**
     * 菜单属性
     */
    @Column(name = "data_name")
    private String dataName;

    /**
     * 菜单新页面打开
     */
    private String target;

    /**
     * 菜单排序号
     */
    private Integer sort;

    /**
     * 是否删除 0未删除 1已删除
     */
    private Integer isdelete;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    public SysMenu withId(Integer id) {
        this.setId(id);
        return this;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取菜单父id
     *
     * @return parent_id - 菜单父id
     */
    public Integer getParentId() {
        return parentId;
    }

    public SysMenu withParentId(Integer parentId) {
        this.setParentId(parentId);
        return this;
    }

    /**
     * 设置菜单父id
     *
     * @param parentId 菜单父id
     */
    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    /**
     * 获取菜单图标
     *
     * @return icon - 菜单图标
     */
    public String getIcon() {
        return icon;
    }

    public SysMenu withIcon(String icon) {
        this.setIcon(icon);
        return this;
    }

    /**
     * 设置菜单图标
     *
     * @param icon 菜单图标
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * 获取菜单标题
     *
     * @return title - 菜单标题
     */
    public String getTitle() {
        return title;
    }

    public SysMenu withTitle(String title) {
        this.setTitle(title);
        return this;
    }

    /**
     * 设置菜单标题
     *
     * @param title 菜单标题
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * 获取菜单外部链接
     *
     * @return href - 菜单外部链接
     */
    public String getHref() {
        return href;
    }

    public SysMenu withHref(String href) {
        this.setHref(href);
        return this;
    }

    /**
     * 设置菜单外部链接
     *
     * @param href 菜单外部链接
     */
    public void setHref(String href) {
        this.href = href;
    }

    /**
     * 获取菜单链接
     *
     * @return lay_href - 菜单链接
     */
    public String getLayHref() {
        return layHref;
    }

    public SysMenu withLayHref(String layHref) {
        this.setLayHref(layHref);
        return this;
    }

    /**
     * 设置菜单链接
     *
     * @param layHref 菜单链接
     */
    public void setLayHref(String layHref) {
        this.layHref = layHref;
    }

    /**
     * 获取菜单事件
     *
     * @return layadmin_event - 菜单事件
     */
    public String getLayadminEvent() {
        return layadminEvent;
    }

    public SysMenu withLayadminEvent(String layadminEvent) {
        this.setLayadminEvent(layadminEvent);
        return this;
    }

    /**
     * 设置菜单事件
     *
     * @param layadminEvent 菜单事件
     */
    public void setLayadminEvent(String layadminEvent) {
        this.layadminEvent = layadminEvent;
    }

    /**
     * 获取菜单展开页标题
     *
     * @return lay_text - 菜单展开页标题
     */
    public String getLayText() {
        return layText;
    }

    public SysMenu withLayText(String layText) {
        this.setLayText(layText);
        return this;
    }

    /**
     * 设置菜单展开页标题
     *
     * @param layText 菜单展开页标题
     */
    public void setLayText(String layText) {
        this.layText = layText;
    }

    /**
     * 获取菜单属性
     *
     * @return data_name - 菜单属性
     */
    public String getDataName() {
        return dataName;
    }

    public SysMenu withDataName(String dataName) {
        this.setDataName(dataName);
        return this;
    }

    /**
     * 设置菜单属性
     *
     * @param dataName 菜单属性
     */
    public void setDataName(String dataName) {
        this.dataName = dataName;
    }

    /**
     * 获取菜单新页面打开
     *
     * @return target - 菜单新页面打开
     */
    public String getTarget() {
        return target;
    }

    public SysMenu withTarget(String target) {
        this.setTarget(target);
        return this;
    }

    /**
     * 设置菜单新页面打开
     *
     * @param target 菜单新页面打开
     */
    public void setTarget(String target) {
        this.target = target;
    }

    /**
     * 获取菜单排序号
     *
     * @return sort - 菜单排序号
     */
    public Integer getSort() {
        return sort;
    }

    public SysMenu withSort(Integer sort) {
        this.setSort(sort);
        return this;
    }

    /**
     * 设置菜单排序号
     *
     * @param sort 菜单排序号
     */
    public void setSort(Integer sort) {
        this.sort = sort;
    }

    /**
     * 获取是否删除 0未删除 1已删除
     *
     * @return isdelete - 是否删除 0未删除 1已删除
     */
    public Integer getIsdelete() {
        return isdelete;
    }

    public SysMenu withIsdelete(Integer isdelete) {
        this.setIsdelete(isdelete);
        return this;
    }

    /**
     * 设置是否删除 0未删除 1已删除
     *
     * @param isdelete 是否删除 0未删除 1已删除
     */
    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", parentId=").append(parentId);
        sb.append(", icon=").append(icon);
        sb.append(", title=").append(title);
        sb.append(", href=").append(href);
        sb.append(", layHref=").append(layHref);
        sb.append(", layadminEvent=").append(layadminEvent);
        sb.append(", layText=").append(layText);
        sb.append(", dataName=").append(dataName);
        sb.append(", target=").append(target);
        sb.append(", sort=").append(sort);
        sb.append(", isdelete=").append(isdelete);
        sb.append("]");
        return sb.toString();
    }
}