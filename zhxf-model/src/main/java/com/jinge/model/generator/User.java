package com.jinge.model.generator;

import java.io.Serializable;

import javax.persistence.*;

@Table(name = "user")
public class User implements Serializable{
    /**  
     * serialVersionUID:TODO(用一句话描述这个变量表示什么).  
     */
    private static final long serialVersionUID = 1468272171004101602L;

    @Id
    @GeneratedValue(generator = "JDBC")
    private Integer id;

    /**
     * 登录名
     */
    private String username;

    /**
     * 密码
     */
    private String password;

    /**
     * 头像地址
     */
    private String headurl;

    /**
     * 角色id
     */
    @Column(name = "role_id")
    private Integer roleId;

    /**
     * 是否删除 0未删除 1已删除
     */
    private Integer isdelete;

    /**
     * @return id
     */
    public Integer getId() {
        return id;
    }

    public User withId(Integer id) {
        this.setId(id);
        return this;
    }

    /**
     * @param id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * 获取登录名
     *
     * @return username - 登录名
     */
    public String getUsername() {
        return username;
    }

    public User withUsername(String username) {
        this.setUsername(username);
        return this;
    }

    /**
     * 设置登录名
     *
     * @param username 登录名
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * 获取密码
     *
     * @return password - 密码
     */
    public String getPassword() {
        return password;
    }

    public User withPassword(String password) {
        this.setPassword(password);
        return this;
    }

    /**
     * 设置密码
     *
     * @param password 密码
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * 获取头像地址
     *
     * @return headurl - 头像地址
     */
    public String getHeadurl() {
        return headurl;
    }

    public User withHeadurl(String headurl) {
        this.setHeadurl(headurl);
        return this;
    }

    /**
     * 设置头像地址
     *
     * @param headurl 头像地址
     */
    public void setHeadurl(String headurl) {
        this.headurl = headurl;
    }

    /**
     * 获取角色id
     *
     * @return role_id - 角色id
     */
    public Integer getRoleId() {
        return roleId;
    }

    public User withRoleId(Integer roleId) {
        this.setRoleId(roleId);
        return this;
    }

    /**
     * 设置角色id
     *
     * @param roleId 角色id
     */
    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }

    /**
     * 获取是否删除 0未删除 1已删除
     *
     * @return isdelete - 是否删除 0未删除 1已删除
     */
    public Integer getIsdelete() {
        return isdelete;
    }

    public User withIsdelete(Integer isdelete) {
        this.setIsdelete(isdelete);
        return this;
    }

    /**
     * 设置是否删除 0未删除 1已删除
     *
     * @param isdelete 是否删除 0未删除 1已删除
     */
    public void setIsdelete(Integer isdelete) {
        this.isdelete = isdelete;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(getClass().getSimpleName());
        sb.append(" [");
        sb.append("Hash = ").append(hashCode());
        sb.append(", id=").append(id);
        sb.append(", username=").append(username);
        sb.append(", password=").append(password);
        sb.append(", headurl=").append(headurl);
        sb.append(", roleId=").append(roleId);
        sb.append(", isdelete=").append(isdelete);
        sb.append("]");
        return sb.toString();
    }
}