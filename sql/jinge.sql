/*
 Navicat Premium Data Transfer

 Source Server         : 127.0.0.1
 Source Server Type    : MySQL
 Source Server Version : 50709
 Source Host           : 127.0.0.1:3306
 Source Schema         : jinge

 Target Server Type    : MySQL
 Target Server Version : 50709
 File Encoding         : 65001

 Date: 06/08/2019 17:40:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for dept
-- ----------------------------
DROP TABLE IF EXISTS `dept`;
CREATE TABLE `dept`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '部门名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dept
-- ----------------------------
INSERT INTO `dept` VALUES (1, '人事部');
INSERT INTO `dept` VALUES (2, '业务部');
INSERT INTO `dept` VALUES (3, '销售部');
INSERT INTO `dept` VALUES (4, '产品部');
INSERT INTO `dept` VALUES (5, '测试部');

-- ----------------------------
-- Table structure for dingtalk_user
-- ----------------------------
DROP TABLE IF EXISTS `dingtalk_user`;
CREATE TABLE `dingtalk_user`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `unionid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工在当前开发者企业账号范围内的唯一标识',
  `userid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工在当前企业内的唯一标识，也称staffId。可由企业在创建时指定，并代表一定含义比如工号',
  `name` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工名字',
  `work_place` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '办公地点',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '备注',
  `mobile` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '手机号码',
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工的电子邮箱',
  `org_email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工的企业邮箱',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像url',
  `active` int(2) NOT NULL DEFAULT 1 COMMENT '是否已经激活，1表示已激活，0表示未激活',
  `position` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '职位信息',
  `state_code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '国家地区码',
  `nick` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户昵称',
  `openid` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '用户在当前开放应用内的唯一标识',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '钉钉用户信息表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of dingtalk_user
-- ----------------------------
INSERT INTO `dingtalk_user` VALUES ('e1ae228c-36c0-4d5a-8edc-db75788d1300', '64EojK1FTrLNvctCeCdiiDwiEiE', '', '', '', '', '17305137133', '', '', '', 1, 'java开发', '', '黄岩', '4cFgeDMFiPjD8w1AQEeKD5AiEiE');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工编号',
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '员工姓名',
  `dept_name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '所在部门',
  `base_pay` int(11) NOT NULL DEFAULT 0 COMMENT '基本工资',
  `bonus` int(11) NOT NULL DEFAULT 0 COMMENT '奖金',
  `housing_allowance` int(11) NOT NULL DEFAULT 0 COMMENT '住房补助',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES (2, '1002', '郝思嘉', '行政部', 2000, 300, 100);
INSERT INTO `employee` VALUES (3, '1003', '林晓彤', '财务部', 2500, 360, 100);
INSERT INTO `employee` VALUES (4, '1004', '曾云儿', '销售部', 2000, 450, 100);
INSERT INTO `employee` VALUES (5, '1005', '邱月清', '业务部', 3000, 120, 100);
INSERT INTO `employee` VALUES (6, '1006', '薛伟', '行政部', 2500, 300, 100);
INSERT INTO `employee` VALUES (7, '1007', '曾毅', '行政部', 2800, 360, 100);
INSERT INTO `employee` VALUES (8, '1008', '陶宏开', '销售部', 2200, 450, 100);
INSERT INTO `employee` VALUES (9, '1009', '丁欣', '业务部', 3400, 120, 100);
INSERT INTO `employee` VALUES (10, '1010', '杨晓林', '人事部', 3300, 280, 100);
INSERT INTO `employee` VALUES (11, '1011', '杜媛媛', '财务部', 3800, 220, 100);
INSERT INTO `employee` VALUES (12, '1012', '乔小麦', '财务部', 2400, 180, 100);
INSERT INTO `employee` VALUES (13, '1013', '赵辉', '销售部', 2500, 140, 100);
INSERT INTO `employee` VALUES (14, '1014', '萧钰', '业务部', 2400, 260, 100);
INSERT INTO `employee` VALUES (15, '1015', '蔡晓蓓', '财务部', 3300, 310, 100);
INSERT INTO `employee` VALUES (16, '1016', '沈沉', '人事部', 3500, 480, 100);
INSERT INTO `employee` VALUES (17, '1017', '陈琳', '行政部', 3800, 500, 100);
INSERT INTO `employee` VALUES (18, '1018', '杨晓娟', '行政部', 3500, 330, 100);
INSERT INTO `employee` VALUES (19, '1019', '李如风', '销售部', 2200, 340, 100);
INSERT INTO `employee` VALUES (20, '1020', '张倩', '销售部', 2900, 140, 100);
INSERT INTO `employee` VALUES (21, '1021', '王世凯', '业务部', 2400, 120, 100);
INSERT INTO `employee` VALUES (22, '1022', '周开来', '业务部', 3300, 460, 100);
INSERT INTO `employee` VALUES (24, '1051', '蒋浩源', '业务部', 4500, 500, 100);

-- ----------------------------
-- Table structure for menu
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单编码，排序及父子关系，两位为一层',
  `title` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单名称；title和name都用这个',
  `icon` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `path` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单链接',
  `second_path` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '二级链接',
  `component` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单页面路径',
  `redirect` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '重定向地址，例如首页路径会自动重定向dashboard',
  `isdelete` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0未删除 1已删除',
  `authority` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单所需权限',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 19 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES (1, '01', '示例', 'example', '/example', '', '@/views/layout/Layout', '/example/table', 0, '');
INSERT INTO `menu` VALUES (2, '02', '表格', 'form', '/form/index', '', '@/views/form/index', '', 0, '');
INSERT INTO `menu` VALUES (3, '03', '多层菜单', 'nested', '/nested', '', '@/views/layout/Layout', '/nested/menu1', 0, '');
INSERT INTO `menu` VALUES (4, '04', '系统设置', 'link', '/system', '', '@/views/layout/Layout', '', 0, '');
INSERT INTO `menu` VALUES (5, '0101', '员工信息（表格）', '', 'table', '', '@/views/table/index', '', 0, '');
INSERT INTO `menu` VALUES (6, '0102', '树', '', 'tree', '', '@/views/tree/index', '', 0, '');
INSERT INTO `menu` VALUES (7, '0103', '富文本编辑框', '', 'tinymce', '', '@/views/tinymce/index', '', 0, '');
INSERT INTO `menu` VALUES (8, '0104', '上传头像', '', 'avatarUpload', '', '@/views/upload/avatarUpload', '', 0, '');
INSERT INTO `menu` VALUES (9, '0301', 'Menu1', '', 'menu1', '', '@/views/nested/menu1/index', '', 0, '');
INSERT INTO `menu` VALUES (10, '030101', 'Menu1-1', '', 'menu1-1', '', '@/views/nested/menu1/menu1-1/index', '', 0, '');
INSERT INTO `menu` VALUES (11, '030102', 'Menu1-2', '', 'menu1-2', '', '@/views/nested/menu1/menu1-2/index', '', 0, '');
INSERT INTO `menu` VALUES (12, '030103', 'Menu1-3', '', 'menu1-3', '', '@/views/nested/menu1/menu1-3/index', '', 0, '');
INSERT INTO `menu` VALUES (13, '0302', 'Menu2', '', 'menu2', '', '@/views/nested/menu2/index', '', 0, '');
INSERT INTO `menu` VALUES (14, '03010201', 'Menu1-2-1', '', 'menu1-2-1', '', '@/views/nested/menu1/menu1-2/menu1-2-1/index', '', 0, '');
INSERT INTO `menu` VALUES (15, '03010202', 'Menu1-2-2', '', 'menu1-2-2', '', '@/views/nested/menu1/menu1-2/menu1-2-2/index', '', 0, '');
INSERT INTO `menu` VALUES (16, '0401', '角色管理', '', 'role', '', '@/views/role/index', '', 0, '');
INSERT INTO `menu` VALUES (17, '05', '高德地图', 'eye', '/amap/index', '', '@/views/amap/index', '', 0, '');
INSERT INTO `menu` VALUES (18, '06', '新闻', 'eye', '/news/index', '', '@/views/news/index', '', 0, '');

-- ----------------------------
-- Table structure for news
-- ----------------------------
DROP TABLE IF EXISTS `news`;
CREATE TABLE `news`  (
  `id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '主键',
  `title` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '标题',
  `content` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '内容',
  `create_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '创建人id',
  `creater` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '创建人',
  `create_time` datetime(0) NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '创建时间',
  `update_id` char(36) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '更新人id',
  `updater` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '更新人',
  `update_time` datetime(0) NOT NULL DEFAULT '1970-01-01 00:00:00' COMMENT '更新时间',
  `is_delete` int(1) UNSIGNED NOT NULL DEFAULT 0 COMMENT '是否删除：0未删除 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '新闻表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of news
-- ----------------------------
INSERT INTO `news` VALUES ('7a34104c-4ad6-4113-b503-d3d3906ecca5', '央视一带一路多次出现连云港画面', '<p>连云港港口作为一带一<strong>路出发点</strong>，深受国家<em>重视</em></p>\n<p><img class=\"wscnph\" /></p>\n<p><img class=\"wscnph\" src=\"file/ac930f8a-af3e-4fee-8844-6a50dd34d52b.png\" width=\"500\" /></p>', '1', 'admin', '2019-04-30 14:44:20', '1', 'admin', '2019-07-23 09:05:55', 0);
INSERT INTO `news` VALUES ('wer', '震惊', '加多宝居然与王老吉口味相同', '1', 'admin', '1970-01-01 00:00:00', '1', 'admin', '2019-04-30 14:35:45', 1);

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色名称',
  `description` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '角色描述',
  `menu_codes` varchar(2000) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单权限列表，起止逗号并以逗号隔开',
  `secured` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '后台数据权限',
  `isdelete` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (1, '系统管理员', '拥有管理员权限的账号', '01,0103,02,0104,0401,0101,04,0102,05,06', 'admin', 0);
INSERT INTO `role` VALUES (2, '普通账号', '', '01,0103,02,0301,0104,03,0302,0101,0102,030101,030103', 'normal', 0);
INSERT INTO `role` VALUES (3, '测试', '', '0301,03,030101', 'ceshi', 0);

-- ----------------------------
-- Table structure for sys_menu
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu`  (
  `id` int(8) UNSIGNED NOT NULL AUTO_INCREMENT,
  `parent_id` int(8) UNSIGNED NOT NULL DEFAULT 0 COMMENT '菜单父id',
  `icon` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单图标',
  `title` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单标题',
  `href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单外部链接',
  `lay_href` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单链接',
  `layadmin_event` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单事件',
  `lay_text` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单展开页标题',
  `data_name` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单属性',
  `target` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '菜单新页面打开',
  `sort` int(8) NOT NULL DEFAULT 0 COMMENT '菜单排序号',
  `isdelete` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 115 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
INSERT INTO `sys_menu` VALUES (1, 0, 'layui-icon-home', '主页', '', '', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (2, 1, '', '控制台', '', 'home/console', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (3, 1, '', '主页一', '', 'home/homepage1.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (4, 1, '', '主页二', '', 'home/homepage2.html', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (5, 0, 'layui-icon-component', '组件', '', '', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (6, 5, '', '栅格', '', '', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (7, 6, '', '等比例列表排列', '', 'component/grid/list.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (8, 6, '', '按移动端排列', '', 'component/grid/mobile.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (9, 6, '', '移动桌面端组合', '', 'component/grid/mobile-pc.html', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (10, 6, '', '全端复杂组合', '', 'component/grid/all.html', '', '', '', '', 4, 0);
INSERT INTO `sys_menu` VALUES (11, 6, '', '低于桌面堆叠排列', '', 'component/grid/stack.html', '', '', '', '', 5, 0);
INSERT INTO `sys_menu` VALUES (12, 6, '', '九宫格', '', 'component/grid/speed-dial.html', '', '', '', '', 6, 0);
INSERT INTO `sys_menu` VALUES (13, 5, '', '按钮', '', 'component/button/index.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (14, 5, '', '表单', '', '', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (15, 14, '', '表单元素', '', 'component/form/element.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (16, 14, '', '表单组合', '', 'component/form/group.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (17, 5, '', '导航', '', 'component/nav/index.html', '', '', '', '', 4, 0);
INSERT INTO `sys_menu` VALUES (18, 5, '', '选项卡', '', 'component/tabs/index.html', '', '', '', '', 5, 0);
INSERT INTO `sys_menu` VALUES (19, 5, '', '进度条', '', 'component/progress/index.html', '', '', '', '', 6, 0);
INSERT INTO `sys_menu` VALUES (20, 5, '', '面板', '', 'component/panel/index.html', '', '', '', '', 7, 0);
INSERT INTO `sys_menu` VALUES (21, 5, '', '徽章', '', 'component/badge/index.html', '', '', '', '', 8, 0);
INSERT INTO `sys_menu` VALUES (22, 5, '', '时间线', '', 'component/timeline/index.html', '', '', '', '', 9, 0);
INSERT INTO `sys_menu` VALUES (23, 5, '', '时间线', '', 'component/timeline/index.html', '', '', '', '', 9, 0);
INSERT INTO `sys_menu` VALUES (24, 5, '', '动画', '', 'component/anim/index.html', '', '', '', '', 10, 0);
INSERT INTO `sys_menu` VALUES (25, 5, '', '辅助', '', 'component/auxiliar/index.html', '', '', '', '', 11, 0);
INSERT INTO `sys_menu` VALUES (26, 5, '', '通用弹层', '', '', '', '', '', '', 12, 0);
INSERT INTO `sys_menu` VALUES (27, 26, '', '功能演示', '', 'component/layer/list.html', '', 'layer 功能演示', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (28, 26, '', '特殊示例', '', 'component/layer/special-demo.html', '', 'layer 特殊示例', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (29, 26, '', '风格定制', '', 'component/layer/theme.html', '', 'layer 风格定制', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (30, 5, '', '日期时间', '', '', '', '', '', '', 13, 0);
INSERT INTO `sys_menu` VALUES (31, 30, '', '功能演示一', '', 'component/laydate/demo1.html', '', 'layDate 功能演示一', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (32, 30, '', '功能演示二', '', 'component/laydate/demo2.html', '', 'layDate 功能演示二', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (33, 30, '', '设定主题', '', 'component/laydate/theme.html', '', 'layDate 设定主题', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (34, 30, '', '特殊示例', '', 'component/laydate/special-demo.html', '', 'layDate 特殊示例', '', '', 4, 0);
INSERT INTO `sys_menu` VALUES (35, 5, '', '静态表格', '', 'component/table/static.html', '', '', '', '', 14, 0);
INSERT INTO `sys_menu` VALUES (36, 5, '', '数据表格', '', '', '', '', '', '', 15, 0);
INSERT INTO `sys_menu` VALUES (37, 36, '', '简单数据表格', '', 'component/table/simple.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (38, 36, '', '列宽自动分配', '', 'component/table/auto.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (39, 36, '', '赋值已知数据', '', 'component/table/data.html', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (40, 36, '', '转化静态表格', '', 'component/table/tostatic.html', '', '', '', '', 4, 0);
INSERT INTO `sys_menu` VALUES (41, 36, '', '开启分页', '', 'component/table/page.html', '', '', '', '', 5, 0);
INSERT INTO `sys_menu` VALUES (42, 36, '', '自定义分页', '', 'component/table/resetPage.html', '', '', '', '', 6, 0);
INSERT INTO `sys_menu` VALUES (43, 36, '', '开启头部工具栏', '', 'component/table/toolbar.html', '', '', '', '', 7, 0);
INSERT INTO `sys_menu` VALUES (44, 36, '', '开启合计行', '', 'component/table/totalRow.html', '', '', '', '', 8, 0);
INSERT INTO `sys_menu` VALUES (45, 36, '', '高度最大适应', '', 'component/table/height.html', '', '', '', '', 9, 0);
INSERT INTO `sys_menu` VALUES (46, 36, '', '开启复选框', '', 'component/table/checkbox.html', '', '', '', '', 10, 0);
INSERT INTO `sys_menu` VALUES (47, 36, '', '开启单选框', '', 'component/table/radio.html', '', '', '', '', 11, 0);
INSERT INTO `sys_menu` VALUES (48, 36, '', '开启单元格编辑', '', 'component/table/cellEdit.html', '', '', '', '', 12, 0);
INSERT INTO `sys_menu` VALUES (49, 36, '', '加入表单元素', '', 'component/table/form.html', '', '', '', '', 13, 0);
INSERT INTO `sys_menu` VALUES (50, 36, '', '设置单元格样式', '', 'component/table/style.html', '', '', '', '', 14, 0);
INSERT INTO `sys_menu` VALUES (51, 36, '', '固定列', '', 'component/table/fixed.html', '', '', '', '', 15, 0);
INSERT INTO `sys_menu` VALUES (52, 36, '', '数据操作', '', 'component/table/operate.html', '', '', '', '', 16, 0);
INSERT INTO `sys_menu` VALUES (53, 36, '', '解析任意数据格式', '', 'component/table/parseData.html', '', '', '', '', 17, 0);
INSERT INTO `sys_menu` VALUES (54, 36, '', '监听行事件', '', 'component/table/onrow.html', '', '', '', '', 18, 0);
INSERT INTO `sys_menu` VALUES (55, 36, '', '数据表格的重载', '', 'component/table/reload.html', '', '', '', '', 19, 0);
INSERT INTO `sys_menu` VALUES (56, 36, '', '设置初始排序', '', 'component/table/initSort.html', '', '', '', '', 20, 0);
INSERT INTO `sys_menu` VALUES (57, 36, '', '监听单元格事件', '', 'component/table/cellEvent.html', '', '', '', '', 21, 0);
INSERT INTO `sys_menu` VALUES (58, 36, '', '复杂表头', '', 'component/table/thead.html', '', '', '', '', 22, 0);
INSERT INTO `sys_menu` VALUES (59, 5, '', '分页', '', '', '', '', '', '', 16, 0);
INSERT INTO `sys_menu` VALUES (60, 59, '', '功能演示一', '', 'component/laypage/demo1.html', '', 'layPage 功能演示一', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (61, 59, '', '功能演示二', '', 'component/laypage/demo2.html', '', 'layPage 功能演示二', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (62, 5, '', '上传', '', '', '', '', '', '', 17, 0);
INSERT INTO `sys_menu` VALUES (63, 62, '', '功能演示一', '', 'component/upload/demo1.html', '', '上传功能演示一', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (64, 62, '', '功能演示二', '', 'component/upload/demo2.html', '', '上传功能演示二', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (65, 5, '', '颜色选择器', '', 'component/colorpicker/index.html', '', '', '', '', 18, 0);
INSERT INTO `sys_menu` VALUES (66, 5, '', '滑块组件', '', 'component/slider/index.html', '', '', '', '', 19, 0);
INSERT INTO `sys_menu` VALUES (67, 5, '', '评分', '', 'component/rate/index.html', '', '', '', '', 20, 0);
INSERT INTO `sys_menu` VALUES (68, 5, '', '轮播', '', 'component/carousel/index.html', '', '', '', '', 21, 0);
INSERT INTO `sys_menu` VALUES (69, 5, '', '流加载', '', 'component/flow/index.html', '', '', '', '', 22, 0);
INSERT INTO `sys_menu` VALUES (70, 5, '', '工具', '', 'component/util/index.html', '', '', '', '', 23, 0);
INSERT INTO `sys_menu` VALUES (71, 5, '', '代码修饰', '', 'component/code/index.html', '', '', '', '', 24, 0);
INSERT INTO `sys_menu` VALUES (72, 0, 'layui-icon-template', '页面', '', '', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (73, 72, '', '个人主页', '', 'template/personalpage.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (74, 72, '', '通讯录', '', 'template/addresslist.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (75, 72, '', '客户列表', '', 'template/caller.html', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (76, 72, '', '商品列表', '', 'template/goodslist.html', '', '', '', '', 4, 0);
INSERT INTO `sys_menu` VALUES (77, 72, '', '留言板', '', 'template/msgboard.html', '', '', '', '', 5, 0);
INSERT INTO `sys_menu` VALUES (78, 72, '', '搜索结果', '', 'template/search.html', '', '', '', '', 6, 0);
INSERT INTO `sys_menu` VALUES (79, 72, '', '注册', '', '', '', 'user/reg.html', '', '_blank', 7, 0);
INSERT INTO `sys_menu` VALUES (80, 72, '', '登入', '', '', '', 'login', '', '_blank', 8, 0);
INSERT INTO `sys_menu` VALUES (81, 72, '', '忘记密码', '', '', '', 'user/forget.html', '', '_blank', 9, 0);
INSERT INTO `sys_menu` VALUES (82, 72, '', '404页面不存在', '', 'template/tips/404.html', '', '', '', '', 10, 0);
INSERT INTO `sys_menu` VALUES (83, 72, '', '错误提示', '', 'template/tips/error.html', '', '', '', '', 11, 0);
INSERT INTO `sys_menu` VALUES (84, 72, '', '百度一下', '', '//www.baidu.com/', '', '', '', '', 12, 0);
INSERT INTO `sys_menu` VALUES (85, 72, '', 'layui官网', '', '//www.layui.com/', '', '', '', '', 13, 0);
INSERT INTO `sys_menu` VALUES (86, 72, '', 'layuiAdmin官网', '', '//www.layui.com/admin/', '', '', '', '', 14, 0);
INSERT INTO `sys_menu` VALUES (87, 0, 'layui-icon-app', '应用', '', '', '', '', '', '', 4, 0);
INSERT INTO `sys_menu` VALUES (88, 87, '', '内容系统', '', '', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (89, 88, '', '文章列表', '', 'app/content/list.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (90, 88, '', '分类管理', '', 'app/content/tags.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (91, 88, '', '评论管理', '', 'app/content/comment.html', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (92, 87, '', '社区系统', '', '', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (93, 92, '', '帖子列表', '', 'app/forum/list.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (94, 92, '', '回帖列表', '', 'app/forum/replys.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (95, 87, '', '消息中心', '', 'app/message/index.html', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (96, 87, '', '工单系统', '', 'app/workorder/list.html', '', '', '', '', 4, 0);
INSERT INTO `sys_menu` VALUES (97, 0, 'layui-icon-senior', '高级', '', '', '', '', '', '', 5, 0);
INSERT INTO `sys_menu` VALUES (98, 97, '', 'LayIM 通讯系统', '', '', 'im', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (99, 97, '', 'Echarts集成', '', '', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (100, 99, '', '折线图', '', 'senior/echarts/line.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (101, 99, '', '柱状图', '', 'senior/echarts/bar.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (102, 99, '', '地图', '', 'senior/echarts/map.html', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (103, 0, 'layui-icon-user', '用户', '', '', '', '', '', '', 6, 0);
INSERT INTO `sys_menu` VALUES (104, 103, '', '网站用户', '', 'user/user/list.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (105, 103, '', '后台管理员', '', 'user/administrators/list.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (106, 103, '', '角色管理', '', 'user/administrators/role.html', '', '', '', '', 3, 0);
INSERT INTO `sys_menu` VALUES (107, 0, 'layui-icon-set', '设置', '', '', '', '', '', '', 7, 0);
INSERT INTO `sys_menu` VALUES (108, 107, '', '系统设置', '', '', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (109, 108, '', '网站设置', '', 'set/system/website.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (110, 108, '', '邮件服务', '', 'set/system/email.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (111, 107, '', '我的设置', '', '', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (112, 111, '', '基本资料', '', 'set/user/info.html', '', '', '', '', 1, 0);
INSERT INTO `sys_menu` VALUES (113, 111, '', '修改密码', '', 'set/user/password.html', '', '', '', '', 2, 0);
INSERT INTO `sys_menu` VALUES (114, 0, 'layui-icon-auz', '授权', 'javascript:;', '//www.layui.com/admin/#get', '', '', '', '', 8, 0);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '登录名',
  `password` varchar(64) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '密码',
  `headurl` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '头像地址',
  `role_id` int(11) NOT NULL COMMENT '角色id',
  `isdelete` int(4) NOT NULL DEFAULT 0 COMMENT '是否删除 0未删除 1已删除',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (1, 'admin', 'e10adc3949ba59abbe56e057f20f883e', '/file/4dc818cb-f131-4f00-8bf0-4652200830c8.png', 1, 0);
INSERT INTO `user` VALUES (2, 'test', 'e10adc3949ba59abbe56e057f20f883e', '/jingespringboot/file/baa1bae4-4e21-4414-8c7f-10e567420d32.png', 2, 0);

SET FOREIGN_KEY_CHECKS = 1;
