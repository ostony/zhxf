package com.jinge.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jinge.common.JingeAnnotations.JingeAuthority;

@Controller
@RequestMapping("/home")
public class HomeController
{
    
    @JingeAuthority("LOGGED_IN")
    @RequestMapping("/console")
    public String console()
    {
        return "home/console.html";
    }
}
