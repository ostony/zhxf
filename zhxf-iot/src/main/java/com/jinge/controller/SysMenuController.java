package com.jinge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.dto.SysMenuDto;
import com.jinge.service.SysMenuService;

@RestController
@RequestMapping("/sysmenu")
public class SysMenuController extends BaseController
{
    @Autowired
    private SysMenuService sysMenuService;
    @GetMapping("/get")
    public List<SysMenuDto> get()
    {
        return sysMenuService.getSysMenu(0);
    }
}
