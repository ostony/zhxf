package com.jinge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jinge.common.JingeAnnotations.JingeAuthority;
import com.jinge.service.SysMenuService;

@Controller
public class DefaultController
{
    @Autowired
    private SysMenuService sysMenuService;
    
    @JingeAuthority("LOGGED_IN")
    @RequestMapping({"","/index"})
    public String index(Model model)
    {
        model.addAttribute("menus", sysMenuService.getSysMenu(0));
        return "index.html";
    }
    
    @RequestMapping("/login")
    public String login(Model model)
    {
        return "login.html";
    }
}
