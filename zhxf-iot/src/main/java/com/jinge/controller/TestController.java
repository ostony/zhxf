package com.jinge.controller;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import cn.hutool.core.lang.UUID;
import cn.hutool.core.util.IdUtil;

@RestController
@RequestMapping("/test")
public class TestController extends BaseController
{
    
    @RequestMapping("/sessionid")
    public String sessionid(HttpSession session)
    {
        return session.getId();
    }
    
    @RequestMapping("/set")
    public String set(@RequestParam("value")String value,HttpSession session)
    {
        session.setAttribute("key", value);
        return "success";
    }
    
    @RequestMapping("/get")
    public String get(HttpSession session)
    {
        return session.getAttribute("key").toString();
    }
    
    public static void main(String[] args) {
        System.out.println(IdUtil.simpleUUID());
        System.out.println(IdUtil.simpleUUID().length());
    }
}
