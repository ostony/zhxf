package com.jinge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.dao.generator.EmployeeMapper;
import com.jinge.model.generator.Employee;

@RestController
@RequestMapping("/emp")
public class EmpController {
	
	@Autowired
    private EmployeeMapper employeeMapper;
	
	@RequestMapping(value="/listEmployee",method=RequestMethod.GET)
    public @ResponseBody List<Employee> listEmployee()
    {
        return employeeMapper.selectAll();
    }
	
	@GetMapping("/test")
	public String test()
	{
		return "success";
	}
}
