package com.jinge.dto;

import java.util.List;

import com.jinge.model.generator.SysMenu;

import lombok.Data;

@Data
public class SysMenuDto
{
    private SysMenu sysMenu;
    
    private List<SysMenuDto> children;
}
