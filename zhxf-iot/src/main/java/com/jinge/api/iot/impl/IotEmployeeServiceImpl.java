package com.jinge.api.iot.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.jinge.api.iot.IotEmployeeService;
import com.jinge.dao.generator.EmployeeMapper;
import com.jinge.model.generator.Employee;

@Component
public class IotEmployeeServiceImpl implements IotEmployeeService
{
    @Autowired
    private EmployeeMapper employeeMapper;
    @Override
    public List<Employee> listEmployee()
    {
        return employeeMapper.selectAll();
    }

}
