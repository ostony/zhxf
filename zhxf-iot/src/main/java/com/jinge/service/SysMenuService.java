package com.jinge.service;

import com.jinge.dao.generator.SysMenuMapper;
import com.jinge.dto.SysMenuDto;
import com.jinge.model.generator.SysMenu;

import tk.mybatis.mapper.entity.Example;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service("sysMenuService")
public class SysMenuService
{
    @Autowired
    private SysMenuMapper sysMenuMapper;
    
    /**
     * 
     * 概要说明 : 获取菜单树形结构. <br>
     * 详细说明 : 获取菜单树形结构. <br>
     *
     * @param parentId
     * @return  List<SysMenuDto> 类型返回值说明
     * @see  com.jinge.service.SysMenuService#getSysMenu()
     * @author  by huangyan @ 2019年8月6日, 下午3:20:41
     */
    public List<SysMenuDto> getSysMenu(Integer parentId)
    {
        Example example = new Example(SysMenu.class);
        example.createCriteria().andEqualTo("parentId",parentId)
        .andEqualTo("isdelete",0);
        example.setOrderByClause("sort asc");
        List<SysMenu> sysMenuList = sysMenuMapper.selectByExample(example);
        List<SysMenuDto> result = new ArrayList<SysMenuDto>();
        for(SysMenu sysMenu : sysMenuList)
        {
            SysMenuDto sysMenuDto = new SysMenuDto();
            sysMenuDto.setSysMenu(sysMenu);
            sysMenuDto.setChildren(getSysMenu(sysMenu.getId()));
            result.add(sysMenuDto);
        }
        return result;
    }
}
