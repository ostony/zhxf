package com.jinge.api.iot;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.jinge.api.iot.fallback.IotEmpServiceFallback;
import com.jinge.model.generator.Employee;

@FeignClient(value="iot",path="/zhxf/iot/emp")
public interface IotEmpService {
	
	@RequestMapping(value="/listEmployee",method=RequestMethod.GET)
	public List<Employee> listEmployee();
	
	@GetMapping("/test")
	public String test();
}
