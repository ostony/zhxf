package com.jinge.controller;

import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController
{
    
    /**
     * 
     * 概要说明 : 查看当前登录用户的信息. <br>
     * 详细说明 : 查看当前登录用户的信息. <br>
     *
     * @return  ApiResponse 类型返回值说明
     * @see  com.jinge.controller.UserController#getUserInfo()
     * @author  by huangyan @ 2019年4月8日, 下午4:20:24
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Secured("ROLE_LOGIN")
    @RequestMapping("/getCurrentUserInfo")
    public Object getCurrentUserInfo()
    {
        return getCurrentUser();
    }
}
