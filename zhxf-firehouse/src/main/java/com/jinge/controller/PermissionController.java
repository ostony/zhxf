package com.jinge.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jinge.common.JingeAnnotations.JingeAuthority;
import com.jinge.common.Result;

@Controller
@RequestMapping("/permission")
public class PermissionController extends BaseController
{
    
    @RequestMapping("/pass")
    @JingeAuthority("admin")
    public String pass()
    {
        return "permission.html";
    }
    
    @RequestMapping("/not-pass")
    @JingeAuthority("admin1")
    public String notPass()
    {
        return "permission.html";
    }
    
    @RequestMapping("/pass-json")
    @JingeAuthority("admin")
    @ResponseBody
    public Result passJson()
    {
        return Result.success();
    }
    
    @RequestMapping("/not-pass-json")
    @JingeAuthority("admin1")
    @ResponseBody
    public Result notPassJson()
    {
        return Result.success();
    }
}
