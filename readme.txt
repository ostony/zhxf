﻿model-- 存放公用model
mapper -- 存放公用mapper配置文件及dao
common-- 公用的工具类，比如jsonutil之类
api -- dubbo暴露出去的接口
login -- 单点登录 port：10001
iot -- 物联网模块 port：10002
vehicle -- 车辆模块 port：10003
firehouse -- 微型消防站 port：10004
...

依赖关系 如下：

login->api->common->mapper->model

iot->api->common->mapper->model

vehicle->api->common->mapper->model

firehouse->api->common->mapper->model
