package com.jinge.api.iot;

import java.util.List;

import com.jinge.model.generator.Employee;

public interface IotEmployeeService
{
    public List<Employee> listEmployee();
}
