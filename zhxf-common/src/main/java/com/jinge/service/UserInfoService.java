package com.jinge.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.jinge.dao.generator.RoleMapper;
import com.jinge.dao.generator.UserMapper;
import com.jinge.model.SecurityUser;
import com.jinge.model.generator.Role;
import com.jinge.model.generator.User;

import tk.mybatis.mapper.entity.Example;


/**
 * 
 * 概要说明 : security认证+授权.  <br>
 * 详细说明 : security认证+授权.  <br>
 * 创建时间 : 2019年4月8日 上午11:05:02 <br>
 * @author  by huangyan
 */
@Service("userInfoService")
public class UserInfoService implements UserDetailsService 
{
    /**
     * 用户mapper
     */
    @Autowired
    private UserMapper userMapper;
	@Autowired
	private RoleMapper roleMapper;
	
	/**
	 * 根据username获取用户信息
	 */
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException 
	{
		User user = findByUsername(username);
		if(user != null)
		{
			//获取到登陆信息，用户名相同默认获取第一个用户，因为注册时需要校验用户名唯一性
			List<SimpleGrantedAuthority> authorities = new ArrayList<>();
			authorities.add(new SimpleGrantedAuthority("ROLE_LOGIN"));
			Role role = roleMapper.selectByPrimaryKey(user.getRoleId());
			if(role != null)
			{
			    //controller接口权限分配
			    authorities.add(new SimpleGrantedAuthority(role.getSecured()));
			}
			return new SecurityUser(user, authorities);
		} else {
			throw new UsernameNotFoundException("用户未注册");
		}
	}
	
	/**
     * 
     * 概要说明 : 根据用户名查找用户信息. <br>
     * 详细说明 : 根据用户名查找用户信息. <br>
     *
     * @param username  登录名
     * @return  User 用户信息
     * @see  com.jinge.service.UserService#findByUsername()
     * @author  by huangyan @ 2019年4月8日, 上午11:13:39
     */
    public User findByUsername(String username) 
    {
        Example userExample = new Example(User.class);
        userExample.createCriteria().andEqualTo("username",username)
            .andEqualTo("isdelete",0);
        List<User> userList = 
                userMapper.selectByExample(userExample);
        if(userList.size() > 0) 
        {
            return userList.get(0);
        }
        return null;
    }
    
    public User findByUsernameAndPassword(String username,String password) 
    {
        Example userExample = new Example(User.class);
        userExample.createCriteria().andEqualTo("username",username)
            .andEqualTo("password",password)
            .andEqualTo("isdelete",0);
        return userMapper.selectOneByExample(userExample);
    }
}
