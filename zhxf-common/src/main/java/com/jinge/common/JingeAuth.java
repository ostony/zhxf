package com.jinge.common;

import java.io.Serializable;
import java.util.List;

import com.jinge.model.generator.Role;
import com.jinge.model.generator.User;

public class JingeAuth implements Serializable
{
    /**  
     * serialVersionUID:TODO(用一句话描述这个变量表示什么).  
     */
    private static final long serialVersionUID = -8411055341927511982L;
    /**
     * 用户代理
     */
    private String userAgent;
    /**
     * 用户信息
     */
    private User user;
    /**
     * 用户角色
     */
    private Role role;
    /**
     * 用户权限
     */
    private List<String> authorities;
    
    /**
     * 过期时间
     */
    private long expireTime;
    
    public long getExpireTime()
    {
        return expireTime;
    }
    public void setExpireTime(long expireTime)
    {
        this.expireTime = expireTime;
    }
    public String getUserAgent()
    {
        return userAgent;
    }
    public void setUserAgent(String userAgent)
    {
        this.userAgent = userAgent;
    }
    public User getUser()
    {
        return user;
    }
    public void setUser(User user)
    {
        this.user = user;
    }
    public Role getRole()
    {
        return role;
    }
    public void setRole(Role role)
    {
        this.role = role;
    }
    public List<String> getAuthorities()
    {
        return authorities;
    }
    public void setAuthorities(List<String> authorities)
    {
        this.authorities = authorities;
    }
    
}
