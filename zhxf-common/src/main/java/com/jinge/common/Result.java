package com.jinge.common;

import java.io.Serializable;

/**
 * 
 * 概要说明 : api接口通用返回类.  <br>
 * 详细说明 : api接口通用返回类.  <br>
 * 创建时间 : 2019年6月5日 下午2:40:46 <br>
 * @author  by huangyan
 */
public class Result implements Serializable
{

    /**  
     * serialVersionUID
     */
    private static final long serialVersionUID = 7975406491332306033L;
    
    private Integer code;
    
    private String msg;
    
    private Object data;
    
    public Result() 
    {
        
    }
    
    public Result(Integer code, String msg)
    {
        this.code = code;
        this.msg = msg;
    }

    public static Result success()
    {
        Result result = new Result();
        result.setResultCode(ResultCode.SUCCESS);
        return result;
    }

    public static Result success(Object data)
    {
        Result result = new Result();
        result.setResultCode(ResultCode.SUCCESS);
        result.setData(data);
        return result;
    }

    public static Result failure(ResultCode resultCode)
    {
        Result result = new Result();
        result.setResultCode(resultCode);
        return result;
    }

    public static Result failure(ResultCode resultCode, Object data)
    {
        Result result = new Result();
        result.setResultCode(resultCode);
        result.setData(data);
        return result;
    }

    public void setResultCode(ResultCode code)
    {
        this.code = code.code();
        this.msg = code.message();
    }

    public Integer getCode()
    {
        return code;
    }

    public void setCode(Integer code)
    {
        this.code = code;
    }

    public String getMsg()
    {
        return msg;
    }

    public void setMsg(String msg)
    {
        this.msg = msg;
    }

    public Object getData()
    {
        return data;
    }

    public void setData(Object data)
    {
        this.data = data;
    }
    
}
