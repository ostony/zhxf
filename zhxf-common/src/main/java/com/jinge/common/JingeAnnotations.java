package com.jinge.common;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

public class JingeAnnotations
{
    
    /**
     * 
     * 概要说明 : 权限注解.  <br>
     * 详细说明 : 权限注解.  <br>
     * 创建时间 : 2019年7月19日 上午11:04:57 <br>
     * @author  by huangyan
     */
    @Retention(RetentionPolicy.RUNTIME)
    public @interface JingeAuthority
    {
        String[] value() default {};
    }
}
