package com.jinge.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jinge.common.Result;
import com.jinge.common.ResultCode;

@Controller
@RequestMapping("/error")
public class ErrorController extends BaseController
{
    
    @RequestMapping("/403")
    public String page403()
    {
        return "403.html";
    }
    
    @RequestMapping("/404")
    public String page404()
    {
        return "error/404.html";
    }
    
    @RequestMapping("/500")
    public String page500(Model model)
    {
        return "500.html";
    }
    
    @RequestMapping("/user-not-logged-in")
    @ResponseBody
    public Result userNotLoggedIn()
    {
        //用户未登录
        return Result.failure(ResultCode.USER_NOT_LOGGED_IN);
    }
    
    @RequestMapping("/permission-no-access")
    @ResponseBody
    public Result permissionNoAccess()
    {
        //无访问权限
        return Result.failure(ResultCode.PERMISSION_NO_ACCESS);
    }
}
