package com.jinge.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.common.JingeAuth;
import com.jinge.common.Result;
import com.jinge.common.ResultCode;
import com.jinge.dao.generator.MenuMapper;
import com.jinge.dao.generator.RoleMapper;
import com.jinge.dao.generator.UserMapper;
import com.jinge.model.generator.Menu;
import com.jinge.model.generator.Role;
import com.jinge.model.generator.User;
import com.jinge.service.UserInfoService;
import com.jinge.util.RedisUtils;

import cn.hutool.core.lang.UUID;
import cn.hutool.crypto.digest.MD5;
import cn.hutool.http.useragent.UserAgent;
import cn.hutool.http.useragent.UserAgentUtil;
import tk.mybatis.mapper.entity.Example;

@RestController
@RequestMapping("/token")
public class TokenController extends BaseController
{
    @Autowired
    private RedisUtils redisUtils;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private MenuMapper menuMapper;
    
    @RequestMapping("/logout")
    public Result logout(HttpServletRequest request)
    {
        Cookie[] cookies = request.getCookies();
        String token = request.getHeader("token");
        if(cookies != null)
        {
            for(Cookie cookie:cookies)
            {
                if("token".equals(cookie.getName()))
                {
                    token = cookie.getValue();
                    break;
                }
            }
        }
        System.out.println(token);
        redisUtils.del(token);
        return Result.success(ResultCode.SUCCESS);
    }
    
    @RequestMapping("/login")
    public Result login(@RequestParam("username")String username,
            @RequestParam("password")String password,
            HttpServletRequest request,
            HttpServletResponse response)
    {
        String userAgentString = request.getHeader("User-Agent");
        //电脑浏览器失效时间2小时，移动端失效时间7天
        long expireTime = 2*3600;
        UserAgent userAgent = UserAgentUtil.parse(userAgentString);
        if(userAgent.isMobile() == true)
        {
            //判断是否是移动端
            expireTime = 7*24*3600;
        }
        password = new MD5().digestHex(password);
        User user = userInfoService.findByUsernameAndPassword(username, password);
        if(user == null)
        {
            //账号不存在或密码错误
            return Result.failure(ResultCode.USER_LOGIN_ERROR);
        }
        //用uuid作为token
        String token = UUID.randomUUID().toString();
        JingeAuth jingeAuth = this.setValue(user,userAgentString,expireTime);
        //放入redis中
        redisUtils.set(token, jingeAuth, expireTime);
        Map<String,String> map = new HashMap<>();
        map.put("token", token);
        response.setHeader("token", token);
        Cookie cookie = new Cookie("token", token);
        response.addCookie(cookie);
        return Result.success(map);
    }
    
    private JingeAuth setValue(User user,String userAgentString,long expireTime)
    {
      //收集用户角色、用户权限
        Role role = roleMapper.selectByPrimaryKey(user.getRoleId());
        List<String> authorities = new ArrayList<>();
        authorities.add("LOGGED_IN");
        authorities.add(role.getSecured());
        Example menuExample = new Example(Menu.class);
        menuExample.createCriteria()
            .andIn("id", Arrays.asList(role.getMenuCodes().split(",")));
        List<Menu> menuList = menuMapper.selectByExample(menuExample);
        for(Menu menu : menuList)
        {
            if(StringUtils.isNotBlank(menu.getAuthority()))
            {
                authorities.add(menu.getAuthority());
            }
        }
        //自定义存储
        JingeAuth jingeAuth = new JingeAuth();
        jingeAuth.setAuthorities(authorities);
        jingeAuth.setRole(role);
        jingeAuth.setUser(user);
        jingeAuth.setUserAgent(userAgentString);
        jingeAuth.setExpireTime(expireTime);
        return jingeAuth;
    }
    
}
