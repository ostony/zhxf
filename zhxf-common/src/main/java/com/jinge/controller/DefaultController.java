package com.jinge.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.jinge.common.JingeAnnotations.JingeAuthority;

@Controller
public class DefaultController
{
    
    @JingeAuthority("LOGGED_IN")
    @RequestMapping("")
    public String index()
    {
        return "index.html";
    }
    
    @RequestMapping("/login")
    public String login(Model model)
    {
        return "login.html";
    }
}
