package com.jinge.util;

import java.io.Serializable;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import com.alibaba.fastjson.JSONObject;

/**
 * 
 * 概要说明 : 消息类.  <br>
 * 详细说明 : 消息类.  <br>
 * 创建时间 : 2019年4月28日 下午5:25:56 <br>
 * @author  by huangyan
 */
public class ApiResponse<T> implements Serializable 
{
	
	/**
	 * @since 2018-8-28 10:24:40
	 */
    private static final long serialVersionUID = 7921415668166548069L;

    /**
     * 状态码
     */
    private String code="200";
	
    /**
     * 消息提示
     */
    private String msg="操作成功";
	
    /**
     * 数据
     */
    private T data=null;
	
    /**
     * 当前时间
     */
    private long timestamp=System.currentTimeMillis();

    public ApiResponse() 
    {
        super();
        this.code = "200";
        this.msg = "操作成功";
        this.timestamp=System.currentTimeMillis();
    }

    public ApiResponse(String code, String msg, T data, long timestamp)
    {
        super();
        this.code = code;
        this.msg = msg;
        this.data = data;
        this.timestamp = timestamp;
    }

    public String getCode() 
    {
        return code;
    }

    public void setCode(String code) 
    {
        this.code = code;
    }

    public String getMsg() 
    {
        return msg;
    }

    public void setMsg(String msg) 
    {
        this.msg = msg;
    }

    public T getData() 
    {
        return data;
    }

    public void setData(T data)
    {
        this.data = data;
    }

    public long getTimestamp() 
    {
        return timestamp;
    }

    public void setTimestamp(long timestamp)
    {
        this.timestamp = timestamp;
    }

    @Override
	public String toString() 
    {
        return ReflectionToStringBuilder.toString(this);
    }
	
    /**
     * 
     * 概要说明 : 转json. <br>
     * 详细说明 : 转json. <br>
     *
     * @return  String 类型返回值说明
     * @see  com.jinge.util.ApiResponse#toJson()
     * @author  by huangyan @ 2019年4月28日, 下午5:25:35
     */
    public String toJson() 
    {
        return JSONObject.toJSONString(this);
    }
	
}
