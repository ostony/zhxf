package com.jinge.util;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ResourceLoader;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import com.jinge.configure.FtpConfig;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.util.UUID;

@Component
public class FtpUtil
{
    @Autowired
    FtpConfig config;
    @Autowired
    private ResourceLoader resourceLoader;
    @Autowired
    FtpPool pool;
    /**
     * Description: 向FTP服务器上传文件
     *
     * @Version2.0
     * @param file
     *            上传到FTP服务器上的文件
     * @return
     *           成功返回文件名，否则返回null
     */
    public  ApiResponse upload(MultipartFile file) throws Exception {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setCode("500");
        apiResponse.setMsg("上传失败！");
        FTPClient ftpClient = pool.getFTPClient();
            //开始进行文件上传
            String fileName=UUID.randomUUID()+file.getOriginalFilename().substring(file.getOriginalFilename().lastIndexOf("."));
            InputStream input=file.getInputStream();
            try {
                boolean result=ftpClient.storeFile(fileName,input);//执行文件传输
                if(!result)
                {//上传失败
                    throw new RuntimeException("上传失败");
                }
                else
                {
                    apiResponse.setCode("200");
                    apiResponse.setMsg("上传成功！");
                    apiResponse.setData(config.workDir+"/"+fileName);
                }
            }catch(Exception e){
                e.printStackTrace();
            }finally {//关闭资源
                input.close();
                System.out.println("开始归还连接");
                pool.returnFTPClient(ftpClient);//归还资源
            }
            return apiResponse;
    }
    /**
     * Description: 从FTP服务器下载文件
     *
     * @Version1.0
     * @param fileName
     *        FTP服务器中的文件名
     * @param resp
     *        响应客户的响应体
     */
    public  void downLoad(String fileName,HttpServletResponse resp) throws IOException {
            FTPClient ftpClient = pool.getFTPClient();
            resp.setContentType("application/force-download");// 设置强制下载不打开 MIME
            resp.addHeader("Content-Disposition", "attachment;fileName="+fileName);// 设置文件名
            //将文件直接读取到响应体中
            OutputStream out = resp.getOutputStream();
            ftpClient.retrieveFile(config.getWorkDir()+"/"+fileName, out);
            out.flush();
            out.close();
            pool.returnFTPClient(ftpClient);
    }
    /**
     * Description: 从FTP服务器读取图片
     *
     * @Version1.0
     * @param fileName
     *        需要读取的文件名
     * @return
     *        返回文件对应的Entity
     */
    public ResponseEntity show(String fileName){
        String username=config.getUsername();
        String password=config.getPassword();
        String host=config.getHost();
        String work=config.getWorkDir();
          //ftp://root:root@192.168.xx.xx/+fileName
        return ResponseEntity.ok(resourceLoader.getResource("ftp://"+username+":"+password+"@"+host+work+"/"+fileName));
    }
}
