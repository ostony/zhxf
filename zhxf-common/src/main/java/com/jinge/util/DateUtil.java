package com.jinge.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * 
 * 概要说明 : 时间、字符串处理类.  <br>
 * 详细说明 : 时间、字符串处理类.  <br>
 * 创建时间 : 2019年4月28日 下午5:28:20 <br>
 * @author  by huangyan
 */
public class DateUtil extends org.apache.commons.lang3.time.DateUtils
{
    /**
     * 月份格式
     */
    private static SimpleDateFormat mon = new SimpleDateFormat("yyyy-MM");
    /**
     * 日期格式
     */
    private static SimpleDateFormat day = new SimpleDateFormat("yyyy-MM-dd");
    /**
     * 日期格式
     */
    private static SimpleDateFormat daySimple = new SimpleDateFormat("yyyyMMdd");
    /**
     * 分钟格式
     */
    private static SimpleDateFormat sec = new SimpleDateFormat("yyyy-MM-dd HH:mm");
    /**
     * 秒格式
     */
    private static SimpleDateFormat all = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    /**
     * 秒格式
     */
    private static SimpleDateFormat smooth = new SimpleDateFormat("yyyyMMddHHmmss");
    /**
     * 毫秒格式
     */
    private static SimpleDateFormat smoothMilli = new SimpleDateFormat("yyyyMMddHHmmssSSS");
    
    private static String[] parsePatterns = {
            "yyyy-MM-dd", "yyyy-MM-dd HH:mm:ss", "yyyy-MM-dd HH:mm", "yyyy-MM", 
            "yyyy/MM/dd", "yyyy/MM/dd HH:mm:ss", "yyyy/MM/dd HH:mm", "yyyy/MM",
            "yyyy.MM.dd", "yyyy.MM.dd HH:mm:ss", "yyyy.MM.dd HH:mm", "yyyy.MM"};
	/**
	 * 年月
	 * @return yyyy-MM
	 */
    public static String getMonth()
    {
        Date date = new Date();
        String d = mon.format(date);
        return d;
    }

	/**
	 * 年月日
	 * @return yyyy-MM-dd
	 */
    public static String getDate()
    {
        Date date = new Date();
        String d = day.format(date);
        return d;
    }
	
	/**
	 * 获取年月日
	 * @return yyyyMMdd
	 */
    public static String getDaySimple()
    {
        Date date = new Date();
        String d = daySimple.format(date);
        return d;
    }
	
	/**
	 * 获取年月日时分
	 * @return yyyy-MM-dd HH:mm
	 */
    public static String getSeconds()
    {
        Date date = new Date();
        String d = sec.format(date);
        return d;
    }
	
	/**
	 * 获取年月日时分秒
	 * @return yyyy-MM-dd HH:mm:ss
	 */
    public static String getALLTime()
    {
        Date date = new Date();
        String d = all.format(date);
        return d;
    }
	
	/**
	 * 根据时间戳获取年月日时分秒
	 * @param timestamp
	 * @return yyyy-MM-dd HH:mm:ss
	 */
    public static String getALLTime(Long timestamp)
    {
        Date date = new Date(timestamp * 1000);
        String d = all.format(date);
        return d;
    }

	/**
	 * @param date
	 * @return yyyy-MM-dd HH:mm:ss
	 */
    public static String getALLTime(Date date)
    {
        String d = all.format(date);
        return d;
    }

	/**
	 * 自定义日期类型
	 * @param format
	 * @return
	 */
    public static String getDate(String format)
    {
        SimpleDateFormat dataFormat = new SimpleDateFormat(format);
        Date date = new Date();
        String d = dataFormat.format(date);
        return d;
    }

	/**
	 * 字符串日期格式转Date类型
	 * @param strDate  yyyyMMddHHmmss格式的字符串
	 * @return 转换好的日期
	 */
    public static Date getSmoothTime(String strDate)
    {
        java.util.Date ret = null;
        try 
        {
            ret = smooth.parse(strDate);
        }
        catch (Exception e)
        {
            System.out.println("~~~~~字符串转换日期时发生了异常~~~~~");
        }
        finally
        {

        }
        return ret;
    }
	
	/**
	 * 字符串日期格式转Date类型
	 * @param strDate  yyyy-MM-dd HH:mm:ss格式的日期字符串
	 * @return 转换好的日期
	 */
    public static Date getAllTime(String strDate)
    {
        java.util.Date ret = null;
        try
        {
            ret = all.parse(strDate);
        }
        catch (Exception e)
        {
            System.out.println("~~~~~字符串转换日期时发生了异常~~~~~");
        }
        finally
        {
			
        }
        return ret;
    }
	
	/**
	 * 获取当前星期几
	 * @param date 日期
	 * @return 星期几
	 */
    public static String getWeekOfDate(Date date)
    {
        String[] weekDays = {"星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"};
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int w = cal.get(Calendar.DAY_OF_WEEK) - 1;
        if (w < 0) 
        {
            w = 0;
        }
        return weekDays[w];
    }
	/**
	 * 获取年月日时分秒
	 * @return yyyyMMddHHmmss
	 */
	public static String getSmoothTime() 
	{
		Date date = new Date();
		return smooth.format(date);
	}
	
	/**
	 * 获取年月日时分秒毫秒
	 * @return yyyyMMddHHmmssSSS
	 */
	public static String getSmoothMilliTime() 
	{
		Date date = new Date();
		return smoothMilli.format(date);
	}
	

	/**
	 * 判断当前时间在否在给定两个时间之前
	 * @param star
	 * @param end
	 * @return
	 */
	public static boolean date_range(String star,String end)
	{
		SimpleDateFormat localTime=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		try{
			Date sdate = localTime.parse(star+" 00:00:01");
			Date edate=localTime.parse(end+" 23:59:59");
			long time = new Date().getTime();
			return (time>=sdate.getTime()&& time<=edate.getTime());
		}catch(Exception e){
			return false;
		}
	}
	
	/**
	 * 判断当前时间断是否给定两个时间断之间
	 * @param star
	 * @param end
	 * @return
	 */
	public static boolean time_range(String star,String end)
	{
		SimpleDateFormat localTime=new SimpleDateFormat("yyyy-MM-dd");
		SimpleDateFormat localTime1=new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
		String now = localTime.format(new Date());
		//System.out.println(now);
		try{
			Date sdate = localTime1.parse(now+" "+star+":00");
			Date edate=localTime1.parse(now+" "+end+":59");
			Date nowDate = localTime1.parse(now +" "+ getALLTime().split(" ")[1]);
			return (nowDate.getTime()>=sdate.getTime()&& nowDate.getTime()<=edate.getTime());
		}catch(Exception e){
			return false;
		}
	}
	public static void main(String[] args) {
		//System.out.println(time_range("11:32", "11:32"));
		System.out.println(getALLTime(todayStart()));
		System.out.println(getALLTime(weekStart()));
		System.out.println(getALLTime(monthStart()));
		System.out.println(getALLTime(yearStart()));
		System.out.println(getALLTime(allStart()));
	}
	/**
	 * 根据传入日期和推移天数获取推移的日期
	 * 返回日期格式为 yyyy-MM-dd hh:mm:ss
	 * @param begindate
	 * @return
	 */
	public static String getAnyNextDate(Date begindate,int day)
	{
            String endDate = "";

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");

            Calendar cal = Calendar.getInstance();
            cal.setTime(begindate);
            cal.add(Calendar.DAY_OF_MONTH, day);
            Date resultDate = cal.getTime();
             endDate = sdf.format(resultDate);
            return endDate;
    }
	
	/**
	 * 获取当天起始时间
	 * @return
	 */
	public static Date todayStart() 
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	/**
	 * 获取当天时间
	 * @param hour  0-23
	 * @return
	 */
	public static Date todayStart(int hour) 
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.HOUR_OF_DAY, hour);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	/**
	 * 获取本周起始时间
	 * @return
	 */
	public static Date weekStart() 
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	public static Date monthStart()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	public static Date yearStart()
	{
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(new Date());
		calendar.set(Calendar.MONTH, 0);
		calendar.set(Calendar.DAY_OF_MONTH, 1);
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		return calendar.getTime();
	}
	
	/**
	 * 
	 * 概要说明 : 获取当前年份第一天日期. <br>
	 * 详细说明 : 获取当前年份第一天日期. <br>
	 *
	 * @param year 年份
	 * @return  Date 类型返回值说明
	 * @see  com.jinge.util.DateUtil#yearStart()
	 * @author  by huangyan @ 2019年4月28日, 下午5:32:06
	 */
    public static Date yearStart(int year)
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
	
	/**
	 * 
	 * 概要说明 : 返回2000年1月1号. <br>
	 * 详细说明 : 返回2000年1月1号. <br>
	 *
	 * @return  Date 类型返回值说明
	 * @see  com.jinge.util.DateUtil#allStart()
	 * @author  by huangyan @ 2019年4月28日, 下午5:30:39
	 */
    public static Date allStart()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.set(Calendar.YEAR, 2000);
        calendar.set(Calendar.MONTH, 0);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
    }
    
    public static Date parseDate(Object str)
    {
        if (str == null)
        {
            return null;
        }
        try
        {
            return parseDate(str.toString(), parsePatterns);
        }
        catch (ParseException e)
        {
            return null;
        }
    }
}
