package com.jinge.configure;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.util.DigestUtils;
import org.springframework.web.accept.ContentNegotiationStrategy;

import com.jinge.model.SecurityUser;
import com.jinge.service.UserInfoService;
import com.jinge.util.ApiResponse;

/**
 * 
 * 概要说明 : 权限配置.  <br>
 * 详细说明 : 权限配置.  <br>
 * 创建时间 : 2019年4月11日 下午1:41:46 <br>
 * @author  by huangyan
 */
//@Configuration
//@EnableWebSecurity
//@EnableGlobalMethodSecurity(securedEnabled = true) // 控制权限注解
public class WebSecurityConfig /*extends WebSecurityConfigurerAdapter*/ {
    /**
     * UserDetailsService接口的实现
     */
    //@Autowired
	private UserInfoService userInfoService;
	
	/**
	 * security5推荐使用更安全的BCryptPasswordEncoder
	 * 但为了兼容目前大部分以md5加密方案的项目使用md5加密
	 * @return
	 */
    protected void configure(AuthenticationManagerBuilder auth)
	        throws Exception 
    {
        auth.userDetailsService(userInfoService)
			.passwordEncoder(new PasswordEncoder()
			{
				@Override
				public String encode(CharSequence rawPassword) 
				{
				    return DigestUtils.md5DigestAsHex(
							rawPassword.toString().getBytes());
				}

				@Override
				public boolean matches(CharSequence rawPassword, 
						String encodedPassword)
				{
				    return encodedPassword.equals(
							DigestUtils.md5DigestAsHex(
									rawPassword.toString().getBytes()));
				}
				
			});
	}

	protected void configure(HttpSecurity http) throws Exception {
	    http.exceptionHandling().accessDeniedHandler(new AccessDeniedHandler() 
	    {
	        /**
	         * 没有权限的接口调用
	         */
	        @Override
            public void handle(HttpServletRequest request, HttpServletResponse arg1, AccessDeniedException arg2) throws IOException, ServletException
            {
	            arg1.setContentType("application/json;charset=utf-8");
	            PrintWriter out = arg1.getWriter();
	            ApiResponse apiResponse = new ApiResponse();
	            apiResponse.setData(false);
	            apiResponse.setMsg(arg2.getMessage());
	            apiResponse.setCode("403");
	            out.write(apiResponse.toJson());
	            out.flush();
	            out.close();
	        }});
        //http.authorizeRequests().anyRequest().authenticated();
		//配置匿名用户权限
	    http.anonymous().authorities("ANONYMOUS");
	    http.authorizeRequests().and().formLogin().loginPage("/index/login")
			.loginProcessingUrl("/login").usernameParameter("username")
			.passwordParameter("password")
			.successHandler(new AuthenticationSuccessHandler() 
			{
				
				@Override
				public void onAuthenticationSuccess(
						HttpServletRequest arg0, 
						HttpServletResponse arg1, 
						Authentication arg2)
						throws IOException, ServletException 
				{
					arg1.setContentType("application/json;charset=utf-8");
					PrintWriter out = arg1.getWriter();
					SecurityUser securityUser = null;
					Object principal = 
							SecurityContextHolder.getContext().getAuthentication().getPrincipal();
					if(principal != null){
						if(principal instanceof SecurityUser) {
							securityUser = (SecurityUser) principal;
						}
					}
					ApiResponse apiResponse = new ApiResponse();
					apiResponse.setData(securityUser);
					apiResponse.setMsg("登录成功");
					//out.print(apiResponse);
					out.write(apiResponse.toJson());
					out.flush();
					out.close();
				}
			}).failureHandler(new AuthenticationFailureHandler() 
			{
				//验证码失败
				@Override
				public void onAuthenticationFailure(
						HttpServletRequest arg0, 
						HttpServletResponse arg1, 
						AuthenticationException arg2)
						throws IOException, ServletException 
				{
					arg1.setContentType("application/json;charset=utf-8");
					PrintWriter out = arg1.getWriter();
					ApiResponse apiResponse = new ApiResponse();
					apiResponse.setData(false);
					apiResponse.setMsg(arg2.getMessage());
					apiResponse.setCode("500");
					//out.print(apiResponse);
					out.write(apiResponse.toJson());
					out.flush();
					out.close();
				}
			})
			.and().logout().permitAll()
			/*.and()
			.exceptionHandling().accessDeniedHandler(
					new AuthenticationAccessDeniedHandler())*/
			//关闭跨域
			.and().cors().and().csrf().disable();
		
	}

}
