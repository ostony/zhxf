package com.jinge.configure;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfig implements WebMvcConfigurer
{
    @Autowired
    private AutorityInterceptor autorityInterceptor;

    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer)
    {
          
        // TODO Auto-generated method stub  
        WebMvcConfigurer.super.configureContentNegotiation(configurer);
        configurer.defaultContentType(MediaType.APPLICATION_JSON_UTF8);
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry)
    {
        WebMvcConfigurer.super.addInterceptors(registry);
        registry.addInterceptor(autorityInterceptor).addPathPatterns("/**")
        .excludePathPatterns("**.js","**.css,**.ico");
    }

}
