package com.jinge.configure;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

import com.jinge.common.JingeAnnotations.JingeAuthority;

import lombok.extern.slf4j.Slf4j;

//@Aspect
//@Slf4j
//@Component
public class AutorityAop
{
    @Pointcut("execution(public * com.jinge.controller..*.*(..))")
    public void autority(){}
    
    @Around("autority()")
    public Object doBefore(ProceedingJoinPoint joinPoint)
    {
        Object result = null;
        try
        {
            boolean isAutority = joinPoint.getSignature().getDeclaringType()
                .isAnnotationPresent(JingeAuthority.class);
            if(isAutority == true)
            {
                //进行鉴权
                //joinPoint.get
            }
        }
        catch(Exception e)
        {
            //log.error("鉴权发生错误：{}",e.getMessage());
        }
        return result;
    }
}
