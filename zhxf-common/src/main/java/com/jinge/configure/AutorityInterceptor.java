package com.jinge.configure;

import java.util.List;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import com.jinge.common.JingeAnnotations.JingeAuthority;
import com.jinge.common.JingeAuth;
import com.jinge.util.RedisUtils;

import cn.hutool.core.annotation.AnnotationUtil;
import cn.hutool.core.lang.UUID;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class AutorityInterceptor implements HandlerInterceptor
{
    @Autowired
    private RedisUtils redisUtils;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception
    {
        //防止循环重定向
        /*Object interceptorTokenObject = request.getAttribute("interceptorToken");
        String interceptorToken = null;
        if(interceptorTokenObject != null)
        {
            interceptorToken = (String)interceptorTokenObject;
            Object redisInterceptorTokenObject = redisUtils.get(interceptorToken);
            if(redisInterceptorTokenObject != null)
            {
                String redisInterceptorTokenString = (String)redisInterceptorTokenObject;
                if(interceptorToken.equals(redisInterceptorTokenString))
                {
                    redisUtils.del(interceptorToken);
                    return true;
                }
            }
        }
        
        
        
        interceptorToken = UUID.randomUUID().toString();
        //重定向时有60秒缓冲期
        redisUtils.set(interceptorToken, interceptorToken, 60);
        request.setAttribute("interceptorToken", interceptorToken);*/
        Cookie[] cookies = request.getCookies();
        String token = request.getHeader("token");
        if(cookies != null)
        {
            for(Cookie cookie:cookies)
            {
                if("token".equals(cookie.getName()))
                {
                    token = cookie.getValue();
                    break;
                }
            }
        }
        
        Object obj = redisUtils.get(token);
        JingeAuth jingeAuth = null;
        if(obj != null)
        {
            jingeAuth = (JingeAuth)obj;
            //刷新token失效期
            redisUtils.refreshExpireTime(token, jingeAuth.getExpireTime());
        }
        if(handler instanceof HandlerMethod)
        {
            HandlerMethod handlerMethod = (HandlerMethod) handler;
            
            // 获取方法上的注解
            //JingeAuthority jingeAuthority = handlerMethod.getMethod().getAnnotation(JingeAuthority.class);
            JingeAuthority jingeAuthority = AnnotationUtil.getAnnotation(handlerMethod.getMethod(), JingeAuthority.class);
            log.info("方法名称：{}",handlerMethod.getMethod().getName());
            log.info("注解：{}",AnnotationUtil.getAnnotations(handlerMethod.getMethod(), true));
            log.info("注解：{}",AnnotationUtil.getAnnotations(handlerMethod.getMethod(), false));
            // 如果方法上的注解为空 则获取类的注解
            if(jingeAuthority == null)
            {
                jingeAuthority = AnnotationUtil.getAnnotation(handlerMethod.getMethod().getDeclaringClass(), JingeAuthority.class);
                //jingeAuthority = handlerMethod.getMethod().getDeclaringClass().getAnnotation(JingeAuthority.class);
            }
            if(jingeAuthority == null)
            {
                return HandlerInterceptor.super.preHandle(request, response, handler);
            }
            //判断是否跳转页面
            boolean isPage = true;
            ResponseBody responseBody = AnnotationUtil.getAnnotation(handlerMethod.getMethod(),ResponseBody.class);
            log.info("responseBody:",responseBody);
            if(responseBody == null)
            {
                RestController restController = AnnotationUtil.getAnnotation(handlerMethod.getMethod().getDeclaringClass(),RestController.class);
                log.info("restController:",restController);
                if(restController != null)
                {
                    isPage = false;
                }
            }
            else
            {
                isPage = false;
            }
            //当前接口需要鉴权
            
            if(obj == null)
            {
                //未登录或登陆过期
                log.error("未登录或登陆过期：{}",handler);
                if(isPage == true)
                {
                    response.sendRedirect(request.getContextPath()+"/login");
                    //request.getRequestDispatcher(request.getContextPath()+"/index/login").forward(request, response);
                    //return false;
                }
                else
                {
                    response.sendRedirect(request.getContextPath()+"/error/user-not-logged-in");
                    //request.getRequestDispatcher(request.getContextPath()+"/error/user-not-logged-in").forward(request, response);
                    //return false;
                }
            }
            else if(!jingeAuth.getUserAgent().equals(request.getHeader("User-Agent")))
            {
                //token被截取的非法请求
                log.error("token被截取的非法请求：{}",handler);
                if(isPage == true)
                {
                    response.sendRedirect(request.getContextPath()+"/login");
                }
                else
                {
                    response.sendRedirect(request.getContextPath()+"/error/user-not-logged-in");
                }
            }
            else
            {
                boolean isPass = false;
                List<String> authorities = jingeAuth.getAuthorities();
                String[] value = jingeAuthority.value();
                for(String authority:value)
                {
                    if(authorities.contains(authority))
                    {
                        isPass = true;
                        break;
                    }
                }
                if(isPass == true)
                {
                }
                else
                {
                    //权限不足
                    log.error("权限不足：{}",handler);
                    if(isPage == true)
                    {
                        response.sendRedirect(request.getContextPath()+"/error/403");
                        //request.getRequestDispatcher(request.getContextPath()+"/error/403").forward(request, response);
                        //return false;
                    }
                    else
                    {
                        response.sendRedirect(request.getContextPath()+"/error/permission-no-access");
                        //request.getRequestDispatcher(request.getContextPath()+"/error/permission-no-access").forward(request, response);
                        //return false;
                    }
                }
            }
        }
        return true;
        //return HandlerInterceptor.super.preHandle(request, response, handler);
    }

}
