package com.jinge.dao.generator;

import com.jinge.model.generator.SysMenu;
import tk.mybatis.mapper.common.Mapper;

public interface SysMenuMapper extends Mapper<SysMenu> {
}