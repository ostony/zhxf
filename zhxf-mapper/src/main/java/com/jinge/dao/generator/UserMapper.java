package com.jinge.dao.generator;

import com.jinge.model.generator.User;
import tk.mybatis.mapper.common.Mapper;

public interface UserMapper extends Mapper<User> {
}