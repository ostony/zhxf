package com.jinge.dao.generator;

import com.jinge.model.generator.Dept;
import tk.mybatis.mapper.common.Mapper;

public interface DeptMapper extends Mapper<Dept> {
}