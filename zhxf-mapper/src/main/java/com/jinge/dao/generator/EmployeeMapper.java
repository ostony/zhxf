package com.jinge.dao.generator;

import com.jinge.model.generator.Employee;
import tk.mybatis.mapper.common.Mapper;

public interface EmployeeMapper extends Mapper<Employee> {
}