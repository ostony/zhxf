package com.jinge.dao.generator;

import com.jinge.model.generator.News;
import tk.mybatis.mapper.common.Mapper;

public interface NewsMapper extends Mapper<News> {
}