package com.jinge.dao.generator;

import com.jinge.model.generator.Menu;
import tk.mybatis.mapper.common.Mapper;

public interface MenuMapper extends Mapper<Menu> {
}