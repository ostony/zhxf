package com.jinge.dao.generator;

import com.jinge.model.generator.DingtalkUser;
import tk.mybatis.mapper.common.Mapper;

public interface DingtalkUserMapper extends Mapper<DingtalkUser> {
}