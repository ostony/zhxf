package com.jinge.dao.generator;

import com.jinge.model.generator.Role;
import tk.mybatis.mapper.common.Mapper;

public interface RoleMapper extends Mapper<Role> {
}