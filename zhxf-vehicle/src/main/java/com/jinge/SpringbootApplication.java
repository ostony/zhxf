package com.jinge;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import tk.mybatis.spring.annotation.MapperScan;

/**
 * 
 * 概要说明 : springboot启动类.  <br>
 * 详细说明 : springboot启动类.  <br>
 * 创建时间 : 2019年4月28日 下午5:19:41 <br>
 * @author  by huangyan
 */
@SpringBootApplication
@MapperScan({"com.jinge.dao","com.jinge.dao.generator"})
@EnableAutoConfiguration(exclude = {
    org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration.class
})
public class SpringbootApplication extends SpringBootServletInitializer 
{
	
    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) 
    {
        return application.sources(SpringbootApplication.class);
    }
	
    /**
     * 
     * 概要说明 : 主启动类. <br>
     * 详细说明 : 主启动类. <br>
     *
     * @param args  默认参数
     * @throws Exception  void 类型返回值说明
     * @see  com.jinge.SpringbootApplication#main()
     * @author  by huangyan @ 2019年4月28日, 下午5:20:47
     */
    public static void main(String[] args) throws Exception 
    {
        SpringApplication.run(SpringbootApplication.class, args);
    }
}
