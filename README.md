# zhxf

#### 介绍
{
基于SpringBoot的微服务应用。企业项目请看 [https://gitee.com/enterprises](https://gitee.com/enterprises)}

#### 软件架构
SpringBoot 2.1.6.RELEASE
SpringCloudAlibaba 0.2.2.RELEASE
Redis
Nginx
Nacos
ftp


#### 安装教程

1. 启动redis、nginx、nacos、ftp
2. 导入sql文件夹下的数据库文件
3. 按顺序打包zhxf-model、zhxf-mapper、zhxf-common、zhxf-api及其余业务模块

#### 使用说明

1. 自定义token权限认证：
	登录：ip:port/项目名/token/login?username=xxx&pasword=xxx来获取token
	授权：获取角色的权限、菜单的权限添加到authorities中，并将用户信息与authorities存进redis里，key作为token，web端过期时间2小时，移动端过期时间7天，每次调用接口自动延长有效期
	拦截：WebMvcConfig中设置不需要拦截的路径，比如.jpg结尾的文件或其它
	权限控制：在controller需要拦截的方法上加上@JingeAuthority注解，值为需要的权限，满足任意一条权限要求即可访问
	异常跳转：访问页面时，User-Agent校验失败或者无token或者token失效，转向login.html页面，权限不足时转向403.html页面；访问接口时，分别返回错误码20001(用户未登录)和70001(无访问权限)
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)