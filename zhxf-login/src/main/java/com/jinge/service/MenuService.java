package com.jinge.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jinge.dao.generator.MenuMapper;
import com.jinge.dao.generator.RoleMapper;
import com.jinge.model.generator.Menu;
import com.jinge.model.generator.Role;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 * 概要说明 : 菜单业务层.  <br>
 * 详细说明 : 菜单业务层.  <br>
 * 创建时间 : 2019年4月8日 下午5:23:16 <br>
 * @author  by huangyan
 */
@Service("menuService")
public class MenuService
{
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private RoleMapper roleMapper;
    
    /**
     * 
     * 概要说明 : 获取当前用户可查看的菜单列表. <br>
     * 详细说明 : 获取当前用户可查看的菜单列表. <br>
     *
     * @param roleId 角色id
     * @return  List<Menu> 类型返回值说明
     * @see  com.jinge.service.MenuService#getCurrentMenuList()
     * @author  by huangyan @ 2019年4月8日, 下午5:33:16
     */
    public List<Menu> getCurrentMenuList(Integer roleId)
    {
        List<Menu> menuList = new ArrayList<>();
        Role role = roleMapper.selectByPrimaryKey(roleId);
        if(role == null) {
            return menuList;
        }
        //获取菜单code列表
        String menuCodes = role.getMenuCodes();
        String[] codes = menuCodes.split(",");
        List<String> values = new ArrayList<>();
        values.add("-1");
        for(String code : codes) {
            if(StringUtils.isNotEmpty(code)) {
                values.add(code);
            }
        }
        Example menuExample = new Example(Menu.class);
        Criteria criteria = menuExample.createCriteria();
        //设置code范围
        criteria.andIn("code", values);
        criteria.andEqualTo("isdelete",0);
        //设置排序方式
        menuExample.setOrderByClause(" code asc ");
        return menuMapper.selectByExample(menuExample);
    }
    
    /**
     * 
     * 概要说明 : 获取所有菜单列表. <br>
     * 详细说明 : 获取所有菜单列表. <br>
     *
     * @return  List<Menu> 菜单列表
     * @see  com.jinge.service.MenuService#getAllMenuList()
     * @author  by huangyan @ 2019年4月15日, 下午1:54:05
     */
    public List<Menu> getAllMenuList()
    {
        Example menuExample = new Example(Menu.class);
        Criteria criteria = menuExample.createCriteria();
        criteria.andEqualTo("isdelete",0);
        //设置排序方式
        menuExample.setOrderByClause(" code asc ");
        return menuMapper.selectByExample(menuExample);
    }
}
