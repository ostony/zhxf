package com.jinge.service;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jinge.dao.generator.UserMapper;
import com.jinge.model.generator.User;

import tk.mybatis.mapper.entity.Example;

/**
 * 
 * 概要说明 : 用户业务层.  <br>
 * 详细说明 : 用户业务层.  <br>
 * 创建时间 : 2019年4月8日 上午11:09:34 <br>
 * @author  by huangyan
 */
@Service("userService")
public class UserService
{
    /**
     * 日志
     */
    @SuppressWarnings("unused")
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     * 用户mapper
     */
    @Autowired
    private UserMapper userMapper;
    
    /**
     * 
     * 概要说明 : 根据用户名查找用户信息. <br>
     * 详细说明 : 根据用户名查找用户信息. <br>
     *
     * @param username  登录名
     * @return  User 用户信息
     * @see  com.jinge.service.UserService#findByUsername()
     * @author  by huangyan @ 2019年4月8日, 上午11:13:39
     */
    public User findByUsername(String username) 
    {
        Example userExample = new Example(User.class);
        userExample.createCriteria().andEqualTo("username",username)
            .andEqualTo("isdelete",0);
        List<User> userList = 
                userMapper.selectByExample(userExample);
        if(userList.size() > 0) 
        {
            return userList.get(0);
        }
        return null;
    }
    
    /**
     * 
     * 概要说明 : 获取用户信息. <br>
     * 详细说明 : 获取用户信息. <br>
     *
     * @param id 用户id
     * @return  User 用户信息
     * @see  com.jinge.service.UserService#findById()
     * @author  by huangyan @ 2019年4月8日, 下午4:16:22
     */
    public User findById(Integer id)
    {
        return userMapper.selectByPrimaryKey(id);
    }
    
    /**
     * 
     * 概要说明 : 更新用户头像. <br>
     * 详细说明 : 更新用户头像. <br>
     *
     * @param id    用户id
     * @param headurl  用户新头像
     * void 类型返回值说明
     * @see  com.jinge.service.UserService#updateHeadurl()
     * @author  by huangyan @ 2019年4月10日, 下午12:45:40
     */
    public void updateHeadurl(Integer id,String headurl)
    {
        User user = findById(id);
        user.setHeadurl(headurl);
        userMapper.updateByPrimaryKey(user);
    }
    
    public List<User> listUser()
    {
        Example userExample = new Example(User.class);
        userExample.createCriteria().andEqualTo("isdelete",0);
        return userMapper.selectByExample(userExample);
    }
}
