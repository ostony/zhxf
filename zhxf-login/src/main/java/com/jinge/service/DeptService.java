package com.jinge.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jinge.dao.generator.DeptMapper;
import com.jinge.model.generator.Dept;

/**
 * 
 * 概要说明 : 部门业务层.  <br>
 * 详细说明 : 部门业务层.  <br>
 * 创建时间 : 2019年4月9日 下午3:50:13 <br>
 * @author  by huangyan
 */
@Service("deptService")
public class DeptService
{
    @Autowired
    private DeptMapper deptMapper;
    
    /**
     * 
     * 概要说明 : 获取所有部门列表. <br>
     * 详细说明 : 获取所有部门列表. <br>
     *
     * @return  List<Dept> 部门列表
     * @see  com.jinge.service.DeptService#listDept()
     * @author  by huangyan @ 2019年4月9日, 下午3:51:24
     */
    public List<Dept> listDept()
    {
        return deptMapper.selectAll();
    }
}
