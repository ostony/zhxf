package com.jinge.service;

import java.util.Date;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jinge.dao.generator.NewsMapper;
import com.jinge.model.generator.News;
import com.jinge.model.generator.User;
import com.jinge.util.DateUtil;
import com.jinge.util.JingePage;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 * 概要说明 : 新闻业务层.  <br>
 * 详细说明 : 新闻业务层.  <br>
 * 创建时间 : 2019年4月30日 上午11:41:23 <br>
 * @author  by huangyan
 */
@Service("newsService")
public class NewsService
{
    /**
     * 新闻dao接口
     */
    @Autowired
    private NewsMapper newsMapper;
    
    /**
     * 
     * 概要说明 : 分页获取新闻列表. <br>
     * 详细说明 : 分页获取新闻列表. <br>
     *
     * @param pageNumber    页码
     * @param pageSize      单页大小
     * @param title         标题
     * @param createId     创建人
     * @param queryTime     创建时间区间：示例  2019-04-21 - 2019-04-29
     * @return  JingePage 类型返回值说明
     * @see  com.jinge.service.NewsService#pageNews()
     * @author  by huangyan @ 2019年4月30日, 上午10:54:41
     */
    public JingePage pageNews(
            Integer pageNumber,Integer pageSize,
            String title,String createId,String queryTime) 
    {
        //设置查询条件
        Example newsExample = new Example(News.class);
        Criteria criteria = newsExample.createCriteria();
        if(StringUtils.isNotBlank(title))
        {
         // like 条件
            criteria.andLike("title", "%"+title+"%");
        }
        if(StringUtils.isNotBlank(createId))
        {
         // = 条件
            criteria.andEqualTo("createId",createId);
        }
        if(StringUtils.isNotBlank(queryTime)) 
        {
         // = 条件
            String[] times = queryTime.split(" - ");
            Date startTime = DateUtil.parseDate(times[0].trim());
            Date endTime = DateUtil.addDays(DateUtil.parseDate(times[1].trim()),1);
            criteria.andBetween("createTime", startTime, endTime);
        }
        //数据库数据是逻辑删除，因此需要加上删除标识判断
        criteria.andEqualTo("isDelete",0);
        //设置排序方式
        newsExample.setOrderByClause(" create_time desc ");
        //利用pagehelper进行分页
        Page<News> page = PageHelper.startPage(pageNumber, pageSize);
        //查询数据
        newsMapper.selectByExample(newsExample);
        //属性进入Page源码查看
        JingePage jingePage = new JingePage(page);
        return jingePage;
    }
    
    /**
     * 
     * 概要说明 : 删除新闻. <br>
     * 详细说明 : 删除新闻. <br>
     *
     * @param id  void 类型返回值说明
     * @see  com.jinge.service.NewsService#delNews()
     * @author  by huangyan @ 2019年4月30日, 上午11:10:07
     */
    public void delNews(String id)
    {
        //根据新闻id获取新闻
        News news = newsMapper.selectByPrimaryKey(id);
        if(news != null)
        {
            //逻辑删除
            news.setIsDelete(1);
            //以主键为条件更新当前数据
            newsMapper.updateByPrimaryKey(news);
        }
    }
    
    /**
     * 
     * 概要说明 : 保存新闻. <br>
     * 详细说明 : 保存新闻. <br>
     *
     * @param user      当前登陆用户
     * @param id        新闻id
     * @param title     新闻标题
     * @param content   新闻内容
     * void 类型返回值说明
     * @see  com.jinge.service.NewsService#saveNews()
     * @author  by huangyan @ 2019年4月30日, 上午11:13:09
     */
    public void saveNews(User user,String id,String title,String content)
    {
        //新闻对象
        News news = null;
        //是否新增
        Boolean isInsert = false;
        if(StringUtils.isNotBlank(id))
        {
            //id不为空是从数据库取新闻对象
            news = newsMapper.selectByPrimaryKey(id);
        } 
        else
        {
            isInsert = true;
            //新建新闻对象
            news = new News();
            news.setId(UUID.randomUUID().toString());
            news.setIsDelete(0);
            news.setCreateId(user.getId().toString());
            news.setCreater(user.getUsername());
            news.setCreateTime(new Date());
        }
        //填值
        news.setContent(content);
        news.setTitle(title);
        news.setUpdateId(user.getId().toString());
        news.setUpdater(user.getUsername());
        news.setUpdateTime(new Date());
        if(isInsert)
        {
            //新增
            newsMapper.insertSelective(news);
        }
        else
        {
            //更新
            newsMapper.updateByPrimaryKeySelective(news);
        }
    }
}
