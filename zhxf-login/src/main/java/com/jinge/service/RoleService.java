package com.jinge.service;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jinge.dao.generator.RoleMapper;
import com.jinge.model.generator.Role;
import com.jinge.util.JingePage;

import tk.mybatis.mapper.entity.Example;

/**
 * 
 * 概要说明 : 角色管理业务层.  <br>
 * 详细说明 : 角色管理业务层.  <br>
 * 创建时间 : 2019年4月15日 下午1:52:25 <br>
 * @author  by huangyan
 */
@Service("roleService")
public class RoleService
{
    @Autowired
    private RoleMapper roleMapper;
    
    /**
     * 
     * 概要说明 : 分页获取角色列表. <br>
     * 详细说明 : 分页获取角色列表. <br>
     *
     * @param pageNumber    页码
     * @param pageSize      单页大小
     * @return  JingePage 类型返回值说明
     * @see  com.jinge.service.RoleService#pageRole()
     * @author  by huangyan @ 2019年4月15日, 下午1:54:32
     */
    public JingePage pageRole(Integer pageNumber,Integer pageSize)
    {
        Example roleExample = new Example(Role.class);
        roleExample.createCriteria().andEqualTo("isdelete",0);
        Page<JingePage> page = PageHelper.startPage(pageNumber, pageSize);
        roleMapper.selectByExample(roleExample);
        JingePage jingePage = new JingePage(page);
        return jingePage;
    }
    
    public void saveRole(Integer id,String name,String description,String secured)
    {
        Role role = new Role();
        if(id != null) 
        {
            role = roleMapper.selectByPrimaryKey(id);
        }
        else
        {
            role.setIsdelete(0);
            role.setMenuCodes("");
        }
        role.setName(name);
        role.setDescription(description);
        role.setSecured(secured);
        if(id != null) 
        {
            roleMapper.updateByPrimaryKey(role);
        }
        else
        {
            roleMapper.insert(role);
        }
    }
    
    public void deleteRole(Integer id)
    {
        Role role = roleMapper.selectByPrimaryKey(id);
        role.setIsdelete(1);
        roleMapper.updateByPrimaryKey(role);
    }
    
    public void saveSecurity(Integer id,String codes)
    {
        String codesStr[] = codes.split(",");
        Set<String> codeSet = new HashSet<>();
        for(String code:codesStr) 
        {
            for(int i=2;i<=code.length();i+=2)
            {
                codeSet.add(code.substring(0, i));
            }
        }
        String menuCodes = StringUtils.join(codeSet,",");
        Role role = roleMapper.selectByPrimaryKey(id);
        role.setMenuCodes(menuCodes);
        roleMapper.updateByPrimaryKey(role);
    }
}
