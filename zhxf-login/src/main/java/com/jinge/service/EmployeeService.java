package com.jinge.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jinge.dao.generator.EmployeeMapper;
import com.jinge.model.generator.Employee;
import com.jinge.util.JingePage;

import tk.mybatis.mapper.entity.Example;
import tk.mybatis.mapper.entity.Example.Criteria;

/**
 * 
 * 概要说明 : 员工信息业务层.  <br>
 * 详细说明 : 员工信息业务层.  <br>
 * 创建时间 : 2019年4月9日 下午3:17:42 <br>
 * @author  by huangyan
 */
@Service("employeeService")
public class EmployeeService
{
    /**
     * 员工mapper
     */
    @Autowired
    private EmployeeMapper employeeMapper;
    
    /**
     * 
     * 概要说明 : 分页获取员工信息. <br>
     * 详细说明 : 分页获取员工信息. <br>
     *
     * @param pageNumber    页码
     * @param pageSize      每页大小
     * @return  JingePage 类型返回值说明
     * @see  com.jinge.service.EmployeeService#pageEmployee()
     * @author  by huangyan @ 2019年4月9日, 下午3:20:30
     */
    public JingePage pageEmployee(
            Integer pageNumber, Integer pageSize,String keyWord,String deptName) 
    {
        Example employeeExample = new Example(Employee.class);
        Criteria criteria = employeeExample.createCriteria();
        if(StringUtils.isNotBlank(keyWord))
        {
            //姓名匹配
            criteria.andLike("name", "%"+keyWord+"%");
        }
        if(StringUtils.isNotBlank(deptName)&&!"all".equals(deptName))
        {
            //部门匹配
            criteria.andEqualTo("deptName",deptName);
        }
        Page<Employee> page = PageHelper.startPage(pageNumber, pageSize);
        employeeMapper.selectByExample(employeeExample);
        JingePage jingePage = new JingePage(page);
        return jingePage;
    }
    
    /**
     * 
     * 概要说明 : 获取所有员工信息. <br>
     * 详细说明 : 获取所有员工信息. <br>
     *
     * @param keyWord 关键词
     * @param deptName  部门名称
     * @return  List<Employee> 员工列表
     * @see  com.jinge.service.EmployeeService#allEmployee()
     * @author  by huangyan @ 2019年4月15日, 上午9:12:14
     */
    public List<Employee> allEmployee(String keyWord,String deptName) 
    {
        Example employeeExample = new Example(Employee.class);
        Criteria criteria = employeeExample.createCriteria();
        if(StringUtils.isNotBlank(keyWord))
        {
            //姓名匹配
            criteria.andLike("name", "%"+keyWord+"%");
        }
        if(StringUtils.isNotBlank(deptName)&&!"all".equals(deptName))
        {
            //部门匹配
            criteria.andEqualTo("deptName",deptName);
        }
        return employeeMapper.selectByExample(employeeExample);
    }
    
    /**
     * 
     * 概要说明 : 新增员工信息. <br>
     * 详细说明 : 新增员工信息. <br>
     *
     * @param number    员工编号
     * @param name      员工姓名
     * @param deptName  所在部门
     * @param basePay   基本工资
     * @param bonus     奖金
     * @param housingAllowance 住房补助
     *   void 类型返回值说明
     * @see  com.jinge.service.EmployeeService#saveEmployee()
     * @author  by huangyan @ 2019年4月9日, 下午3:53:51
     */
    public void saveEmployee(
            Integer id,
            String number,String name,String deptName,
            Integer basePay,Integer bonus,Integer housingAllowance)
    {
        Employee employee = null;
        if(id == null)
        {
            employee = new Employee();
        }
        else
        {
            employee = employeeMapper.selectByPrimaryKey(id);
        }
        employee.setBasePay(basePay);
        employee.setNumber(number);
        employee.setName(name);
        employee.setDeptName(deptName);
        employee.setBonus(bonus);
        employee.setHousingAllowance(housingAllowance);
        if(employee.getId() == null)
        {
            employeeMapper.insert(employee);
        }
        else
        {
            employeeMapper.updateByPrimaryKey(employee);
        }
    }
    
    /**
     * 
     * 概要说明 : 删除员工信息. <br>
     * 详细说明 : 删除员工信息. <br>
     *
     * @param id  void 类型返回值说明
     * @see  com.jinge.service.EmployeeService#deleteEmployee()
     * @author  by huangyan @ 2019年4月12日, 上午9:48:10
     */
    public void deleteEmployee(Integer id)
    {
        employeeMapper.deleteByPrimaryKey(id);
    }
}
