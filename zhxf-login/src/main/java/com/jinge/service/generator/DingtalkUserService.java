package com.jinge.service.generator;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.jinge.dao.generator.DingtalkUserMapper;
import com.jinge.model.generator.DingtalkUser;

import tk.mybatis.mapper.entity.Example;

/**
 * 
 * 概要说明 : DingtalkUserService.  <br>
 * 详细说明 : DingtalkUserService.  <br>
 * 创建时间 : 2019年6月24日 下午4:34:07 <br>
 * @author  by huangyan
 */
@Service("dingtalkUserService")
public class DingtalkUserService
{
    @Autowired
    private DingtalkUserMapper dingtalkUserMapper;
    
    public DingtalkUser getDingtalkUser(String userid) {
        Example dingtalkUserExample = new Example(DingtalkUser.class);
        dingtalkUserExample.createCriteria().andEqualTo("userid",userid);
        List<DingtalkUser> dingtalkUserList = 
                dingtalkUserMapper.selectByExample(dingtalkUserExample);
        if(dingtalkUserList != null && dingtalkUserList.size() > 0) {
            return dingtalkUserList.get(0);
        } else {
            return null;
        }
    }
    
    public DingtalkUser getDingtalkUserByOpenid(String openid) {
        Example dingtalkUserExample = new Example(DingtalkUser.class);
        dingtalkUserExample.createCriteria().andEqualTo("openid",openid);
        List<DingtalkUser> dingtalkUserList = 
                dingtalkUserMapper.selectByExample(dingtalkUserExample);
        if(dingtalkUserList != null && dingtalkUserList.size() > 0) {
            return dingtalkUserList.get(0);
        } else {
            return null;
        }
    }
}
