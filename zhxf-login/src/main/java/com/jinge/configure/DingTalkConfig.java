package com.jinge.configure;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix="dingding")
@PropertySource("classpath:dingding.properties")
public class DingTalkConfig
{
    public String appKey;
    public String appSecret;
    public String token;
    public String secret;
    public String suiteKey;
    
    public String getToken()
    {
        return token;
    }
    public void setToken(String token)
    {
        this.token = token;
    }
    public String getSecret()
    {
        return secret;
    }
    public void setSecret(String secret)
    {
        this.secret = secret;
    }
    public String getSuiteKey()
    {
        return suiteKey;
    }
    public void setSuiteKey(String suiteKey)
    {
        this.suiteKey = suiteKey;
    }
    public String getAppKey()
    {
        return appKey;
    }
    public void setAppKey(String appKey)
    {
        this.appKey = appKey;
    }
    public String getAppSecret()
    {
        return appSecret;
    }
    public void setAppSecret(String appSecret)
    {
        this.appSecret = appSecret;
    }
}
