package com.jinge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.service.NewsService;
import com.jinge.util.ApiResponse;
import com.jinge.util.JingePage;

/**
 * 
 * 概要说明 : 新闻控制层.  <br>
 * 详细说明 : 新闻控制层.  <br>
 * 创建时间 : 2019年4月30日 上午11:42:43 <br>
 * @author  by huangyan
 */
@RestController
@RequestMapping("/news")
public class NewsController extends BaseController
{
    /**
     * 新闻业务层
     */
    @Autowired
    private NewsService newsService;
    
    /**
     * 
     * 概要说明 : 分页获取新闻列表. <br>
     * 详细说明 : 分页获取新闻列表. <br>
     *
     * @param pageNumber    页码
     * @param pageSize      每页大小
     * @param title         新闻标题
     * @param createId      创建人id
     * @param queryTime     创建时间区间：示例  2019-04-21 - 2019-04-29
     * @return  ApiResponse 类型返回值说明
     * @see  com.jinge.controller.NewsController#pageNews()
     * @author  by huangyan @ 2019年4月30日, 上午11:43:03
     */
    @RequestMapping("/pageNews")
    @SuppressWarnings({ "rawtypes", "unchecked" })
    public ApiResponse pageNews(
            @RequestParam(value = "pageNumber", required = false, defaultValue = "1")
            Integer pageNumber,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10")
            Integer pageSize,
            @RequestParam(value = "title", required = false, defaultValue = "")
            String title,
            @RequestParam(value = "createId", required = false, defaultValue = "")
            String createId,
            @RequestParam(value = "queryTime", required = false, defaultValue = "")
            String queryTime)
    {
        JingePage jingePage = 
                newsService.pageNews(pageNumber, pageSize, title, createId, queryTime);
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(jingePage);
        return apiResponse;
    }
    
    /**
     * 
     * 概要说明 : 删除新闻. <br>
     * 详细说明 : 删除新闻. <br>
     *
     * @param id    新闻id
     * @return  ApiResponse 类型返回值说明
     * @see  com.jinge.controller.NewsController#delNews()
     * @author  by huangyan @ 2019年4月30日, 上午11:43:47
     */
    @RequestMapping("/delNews")
    @SuppressWarnings("rawtypes")
    public ApiResponse delNews(@RequestParam("id")String id) 
    {
        newsService.delNews(id);
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMsg("删除成功！");
        return apiResponse;
    }
    
    /**
     * 
     * 概要说明 : 保存新闻. <br>
     * 详细说明 : 保存新闻. <br>
     *
     * @param id            新闻id
     * @param title         新闻标题
     * @param content       新闻内容
     * @return  ApiResponse 类型返回值说明
     * @see  com.jinge.controller.NewsController#saveNews()
     * @author  by huangyan @ 2019年4月30日, 上午11:44:05
     */
    @RequestMapping("/saveNews")
    @SuppressWarnings("rawtypes")
    public ApiResponse saveNews(
            @RequestParam(value = "id", required = false, defaultValue = "")
            String id,
            @RequestParam("title")String title,
            @RequestParam("content")String content) {
        newsService.saveNews(getCurrentUser(), id, title, content);
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setMsg("保存成功！");
        return apiResponse;
    }
}
