package com.jinge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.service.MenuService;
import com.jinge.util.ApiResponse;

@RestController
@RequestMapping("/menu")
public class MenuController extends BaseController
{
    /**
     * 菜单业务层
     */
    @Autowired
    private MenuService menuService;
    
    /**
     * 
     * 概要说明 : 获取当前用户可查看的菜单列表. <br>
     * 详细说明 : 获取当前用户可查看的菜单列表. <br>
     *
     * @return  ApiResponse 类型返回值说明
     * @see  com.jinge.controller.MenuController#getCurrentMenuList()
     * @author  by huangyan @ 2019年4月8日, 下午5:36:39
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Secured("ROLE_LOGIN")
    @RequestMapping(value="/getCurrentMenuList",produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse getCurrentMenuList()
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(menuService.getCurrentMenuList(getCurrentUser().getRoleId()));
        return apiResponse;
    }
    
    /**
     * 
     * 概要说明 : 获取所有菜单列表. <br>
     * 详细说明 : 获取所有菜单列表. <br>
     *
     * @return  ApiResponse 类型返回值说明
     * @see  com.jinge.controller.MenuController#getAllMenuList()
     * @author  by huangyan @ 2019年4月15日, 下午1:55:49
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping(value="/getAllMenuList",produces = MediaType.APPLICATION_JSON_VALUE)
    public ApiResponse getAllMenuList()
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(menuService.getAllMenuList());
        return apiResponse;
    }
}
