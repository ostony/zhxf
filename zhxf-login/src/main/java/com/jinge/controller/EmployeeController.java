package com.jinge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.service.EmployeeService;
import com.jinge.util.ApiResponse;
import com.jinge.util.JingePage;

/**
 * 
 * 概要说明 : 员工信息管理控制层.  <br>
 * 详细说明 : 员工信息管理控制层.  <br>
 * 创建时间 : 2019年4月12日 上午9:52:19 <br>
 * @author  by huangyan
 */
@RestController
@RequestMapping("/employee")
public class EmployeeController extends BaseController
{
    /**
     * 员工信息管理业务层
     */
    @Autowired
    private EmployeeService employeeService;
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @RequestMapping("/pageEmployee")
    public ApiResponse pageEmployee(
            @RequestParam(value = "pageNumber", required = false, defaultValue = "1")
            Integer pageNumber,
            @RequestParam(value = "pageSize", required = false, defaultValue = "10")
            Integer pageSize,
            @RequestParam(value = "keyWord", required = false, defaultValue = "")
            String keyWord,
            @RequestParam(value = "deptName", required = false, defaultValue = "")
            String deptName)
    {
        JingePage jingePage = employeeService.pageEmployee(
                pageNumber, pageSize,keyWord,deptName);
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(jingePage);
        return apiResponse;
    }
    
    @SuppressWarnings({ "rawtypes"})
    @RequestMapping("/saveEmployee")
    public ApiResponse saveEmployee(
            @RequestParam(value = "id", required = false)Integer id,
            @RequestParam("number")String number,
            @RequestParam("name")String name,
            @RequestParam("deptName")String deptName,
            @RequestParam("basePay")Integer basePay,
            @RequestParam("bonus")Integer bonus,
            @RequestParam("housingAllowance")Integer housingAllowance)
    {
        employeeService.saveEmployee(id,
                number, name, deptName, basePay, bonus, housingAllowance);
        ApiResponse apiResponse = new ApiResponse();
        return apiResponse;
    }
    
    @SuppressWarnings({ "rawtypes"})
    @RequestMapping("/deleteEmployee")
    public ApiResponse deleteEmployee(
            @RequestParam(value = "id")Integer id)
    {
        employeeService.deleteEmployee(id);
        ApiResponse apiResponse = new ApiResponse();
        return apiResponse;
    }
}
