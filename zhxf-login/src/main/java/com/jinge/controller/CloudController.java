package com.jinge.controller;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.api.iot.IotEmpService;
import com.jinge.api.iot.IotEmployeeService;
import com.jinge.model.generator.Employee;

@RestController
@RequestMapping("/cloud")
public class CloudController
{
    //@Reference
    //@Reference
    private IotEmployeeService iotEmployeeService;
    @Resource
    private IotEmpService iotEmpService;
    
    @RequestMapping("/iot-employeeList")
    public List<Employee> iotEmployeeList()
    {
        return iotEmpService.listEmployee();
    }
    @RequestMapping("/test")
    public String test()
    {
        return iotEmpService.test();
    }
    
    
}
