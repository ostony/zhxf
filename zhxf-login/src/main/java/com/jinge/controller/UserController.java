package com.jinge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.service.UserService;
import com.jinge.util.ApiResponse;

@RestController
@RequestMapping("/user")
public class UserController extends BaseController
{
    @Autowired
    private UserService userService;
    
    /**
     * 
     * 概要说明 : 查看当前登录用户的信息. <br>
     * 详细说明 : 查看当前登录用户的信息. <br>
     *
     * @return  ApiResponse 类型返回值说明
     * @see  com.jinge.controller.UserController#getUserInfo()
     * @author  by huangyan @ 2019年4月8日, 下午4:20:24
     */
    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Secured("ROLE_LOGIN")
    @RequestMapping("/getCurrentUserInfo")
    public ApiResponse getCurrentUserInfo()
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(userService.findById(getCurrentUser().getId()));
        return apiResponse;
    }
    
    /**
     * 
     * 概要说明 : 更新用户头像. <br>
     * 详细说明 : 更新用户头像. <br>
     *
     * @param headurl 用户头像
     * @return  ApiResponse 类型返回值说明
     * @see  com.jinge.controller.UserController#updateHeadurl()
     * @author  by huangyan @ 2019年4月10日, 下午12:48:13
     */
    @SuppressWarnings({ "rawtypes" })
    @Secured("ROLE_LOGIN")
    @RequestMapping("/updateHeadurl")
    public ApiResponse updateHeadurl(@RequestParam("headurl") String headurl)
    {
        userService.updateHeadurl(getCurrentUser().getId(),headurl);
        ApiResponse apiResponse = new ApiResponse();
        return apiResponse;
    }
    
    @RequestMapping("/listUser")
    public ApiResponse listUser() 
    {
        ApiResponse apiResponse = new ApiResponse();
        apiResponse.setData(userService.listUser());
        return apiResponse;
    }
}
