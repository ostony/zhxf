package com.jinge.controller;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/index")
public class IndexController
{
    
    @RequestMapping("/logged")
    public String logged(Model model,HttpServletRequest request)
    {
        Cookie[] cookies = request.getCookies();
        String token = request.getHeader("token");
        for(Cookie cookie:cookies)
        {
            if("token".equals(cookie.getName()))
            {
                token = cookie.getValue();
                break;
            }
        }
        model.addAttribute("token", token);
        model.addAttribute("userAgent", request.getHeader("User-Agent"));
        return "logged.html";
    }
}
