package com.jinge.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.jinge.configure.WechatConfig;
import com.jinge.util.RedisUtils;

/**
 * hello
 * @author tom
 * @since 2018-11-30 16:33:00
 */
@RestController
@RequestMapping("/security")
public class HelloController {
	@Autowired
	private WechatConfig wechatConfig;
	@Autowired
	private RedisUtils redisUtils;
	
	
	@RequestMapping("/wechatconfig")
	public String wechatconfig() {
		return "APPID:"+wechatConfig.appid+" APPSECRET:"+wechatConfig.appsecret;
	}
	
	@RequestMapping("/set")
    public boolean set(@RequestParam("key")String key,
            @RequestParam("value")String value) {
	    return redisUtils.set(key, value);
    }
	
	@RequestMapping("/get")
    public Object get(@RequestParam("key")String key) {
        return redisUtils.get(key);
    }
}
