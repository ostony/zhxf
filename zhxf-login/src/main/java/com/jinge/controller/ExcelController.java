package com.jinge.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.jinge.model.generator.Employee;
import com.jinge.service.EmployeeService;
import com.jinge.util.ExcelUtil;

@Controller
@RequestMapping("/excel")
public class ExcelController extends BaseController
{
    /**
     * 员工信息管理业务层
     */
    @Autowired
    private EmployeeService employeeService;
    @RequestMapping("/excelDownloadEmployee")
    public void excelDownloadEmployee(@RequestParam("keyWord") String keyWord,
            @RequestParam("deptName") String deptName,
            HttpServletResponse response) 
            throws IOException{
        List<Employee> employees = employeeService.allEmployee(keyWord, deptName);
        List<List<String>> excelData = new ArrayList<>();

        List<String> head = new ArrayList<>();
        head.add("序号");
        head.add("员工编号");
        head.add("员工姓名");
        head.add("所在部门");
        head.add("基本工资");
        head.add("奖金");
        head.add("住房补助");
        head.add("实发工资");
        excelData.add(head);
        int i = 1;
        for(Employee employee:employees) {
            List<String> row = new ArrayList<String>();
            row.add(i+"");i++;
            row.add(employee.getNumber());
            row.add(employee.getName());
            row.add(employee.getDeptName());
            row.add(employee.getBasePay()+"");
            row.add(employee.getBonus()+"");
            row.add(employee.getHousingAllowance()+"");
            row.add((employee.getBasePay()+employee.getBonus()+employee.getHousingAllowance())+"");
            excelData.add(row);
        }
        String sheetName = "员工信息";
        String fileName = "员工信息.xls";
        ExcelUtil.exportExcel(response, excelData, sheetName, fileName, 15);
    }

}
